package net.islandearth.civilization.tech;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.settlements.Settlement;

public class Technology {
	
	Civilization plugin;
	Settlement settlement;
	String name;
	Player researcher;
	int turns = 0;
	int completion, time;
	TechnologyType type;
	
	List<Integer> cancel = new ArrayList<Integer>();
	
	public Technology(Civilization plugin, Settlement settlement, Player researcher, int completion, int time, TechnologyType type)
	{
		this.plugin = plugin;
		this.settlement = settlement;
		this.name = type.getName();
		this.researcher = researcher;
		this.completion = completion;
		this.time = time;
		this.type = type;
	}
	
	private boolean validate()
	{
		switch(name.toLowerCase())
		{
			case "writing":
				AdvancementProgress pottery = researcher.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/pottery")));
				if(pottery.isDone())
				{
					return true;
				} else {
					researcher.sendMessage(ChatColor.RED + "You need to research Pottery first!");
					return false;
				}
				
			case "wheel":
				AdvancementProgress mining = researcher.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/mining")));
				if(mining.isDone())
				{
					return true;
				} else {
					researcher.sendMessage(ChatColor.RED + "You need to research Mining first!");
					return false;
				}
				
			default:
				return true;
		}
	}
	
	private boolean hasScience()
	{
		return settlement.getScience() >= type.getScience();
	}
	
	public void startResearch()
	{
		if(validate() && hasScience())
		{
			Advancement advancement = Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/" + name));
			AdvancementProgress progress = researcher.getAdvancementProgress(advancement);
			
			if(progress.isDone())
			{
				researcher.sendMessage(ChatColor.RED + "This technology has already been researched!");
				return;
			}
			
			BossBar bossbar = Bukkit.createBossBar(ChatColor.AQUA + "Currently Researching: " + type.getFriendlyName(), BarColor.BLUE, BarStyle.SEGMENTED_20);
			bossbar.setProgress(0);
			
			for(OfflinePlayer op : settlement.getMembers())
			{
				if(op.isOnline())
				{
					Player player = Bukkit.getPlayer(op.getUniqueId());
					bossbar.addPlayer(player);
					bossbar.setVisible(true);
				}
			}
			
			if(progress.getAwardedCriteria().size() >= 1 || settlement.getBoosters() >= 1) 
			{
				double calculate = (double) completion / 2.0;
				completion = (int) Math.rint(calculate);
				
				if(settlement.getBoosters() >= 1 && progress.getAwardedCriteria().size() == 0)
				{
					settlement.setBoosters(settlement.getBoosters() - 1);
					researcher.sendMessage(ChatColor.GREEN + "You used a booster to decrease the time it takes to research this technology!");
				}
			}
			
			TechTree tech = new TechTree(plugin);
			tech.showInfo("research", settlement);
			
			final int original = progress.getAwardedCriteria().size();
			final int completion = this.completion;
			final double ui = (double) (100 / completion) / 100;
			
			cancel.add(Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
				if(original != researcher.getAdvancementProgress(advancement).getAwardedCriteria().size())
				{
					cancel();
					redo(bossbar);
				} else {
					turns++;
					if(turns == completion)
					{
						tech.grantTech(settlement, type);
						bossbar.removeAll();
						bossbar.setVisible(false);
						cancel();
					}
					
					bossbar.setProgress(bossbar.getProgress() + ui);
				}
			}, time, time));
		} else if(!hasScience()) {
			researcher.sendMessage(ChatColor.RED + "You do not have the required science to research this!");
		}
	}
	
	public void cancel()
	{
		Bukkit.getScheduler().cancelTask(cancel.get(0));
	}
	
	private void redo(BossBar bossbar)
	{
		bossbar.setVisible(false);
		cancel.remove(cancel.get(0));
		startResearch();
	}
}
