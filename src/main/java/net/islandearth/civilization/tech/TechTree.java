package net.islandearth.civilization.tech;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.settlements.Settlement;

/**
 * Utils class to send toast information and handle technology.
 * @author SamB440
 */
public class TechTree {
	
	private Civilization plugin;
	private Connection sql;
	private String PREFIX_TECH = "tech/";
	private String PREFIX_INFO = "info/";
	
	public TechTree(Civilization plugin)
	{
		this.plugin = plugin;
		this.sql = plugin.getSql();
	}
	
	/**
	 * Grant technology to a player.
	 * @param name - advancement name
	 * @param player - Bukkit player
	 */
	public void grantTech(String name, Player player) 
	{
		AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + name)));
		if(!progress.isDone())
		{
			for(String criteria : progress.getRemainingCriteria())
			{
				progress.awardCriteria(criteria);
			}
		}
	}
	
	public void revokeTech(TechnologyType type, Player player) 
	{
		AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + type.getName())));
		if(progress.isDone())
		{
			for(String criteria : progress.getAwardedCriteria())
			{
				progress.revokeCriteria(criteria);
			}
		}
	}
	
	public void revokeTech(String name, Player player) {
		AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + name)));
		if(progress.isDone())
		{
			for(String criteria : progress.getAwardedCriteria())
			{
				progress.revokeCriteria(criteria);
			}
		}
	}
	
	public void boost(TechnologyType type, String criteria, Settlement settlement)
	{
		for(OfflinePlayer players : settlement.getMembers())
		{
			if(players.isOnline())
			{
				Player pl = Bukkit.getPlayer(players.getUniqueId());
				Advancement advancement = Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + type.getName()));
				AdvancementProgress progress = pl.getAdvancementProgress(advancement);
				if(!progress.getAwardedCriteria().contains(criteria) && !progress.isDone()) 
				{
					progress.awardCriteria(criteria);
					if(plugin.isSQL())
					{
						try {
							PreparedStatement statement = sql.prepareStatement("INSERT INTO Tech (settlement, tech, progress) VALUES (?, ?, ?)");
							statement.setString(1, settlement.getName());
							statement.setString(2, type.toString());
							statement.setInt(3, 1);
							statement.executeUpdate();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} else {
						File save = new File(plugin.getDataFolder() + "/settlements/" + settlement.getName() + ".settlement");
						FileConfiguration yaml = YamlConfiguration.loadConfiguration(save);
						List<String> current = yaml.getStringList("progress");
						current.add(type.toString());
						yaml.set("progress", current);
						reloadConfiguration(save);
					}
				}
			}
		}
	}
	
	public void boost(TechnologyType type, String criteria, Player player)
	{
		Advancement advancement = Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + type.getName()));
		AdvancementProgress progress = player.getAdvancementProgress(advancement);
		if(!progress.getAwardedCriteria().contains(criteria) && !progress.isDone()) 
		{
			progress.awardCriteria(criteria);
		}
	}
	
	/**
	 * Sync a players data after changes were made whilst they were offline.
	 * @param player - Civilization player
	 */
	public void sync(CivPlayer player)
	{
		if (player.getSettlement().getTech() != null) {
			for(TechnologyType tech : player.getSettlement().getTech())
			{
				grantTech(tech.getName(), player.getBukkitPlayer());
			}
			
			for(TechnologyType tech : player.getSettlement().getProgresses())
			{
				boost(tech, tech.getCriteria(), player.getBukkitPlayer());
			}
		}
	}
	
	/**
	 * Grant technology to a player.
	 * @param advancement - Bukkit advancement
	 * @param player - Bukkit player
	 */
	public void grantTech(Advancement advancement, Player player) 
	{
		AdvancementProgress progress = player.getAdvancementProgress(advancement);
		if(!progress.isDone())
		{
			for(String criteria : progress.getRemainingCriteria())
			{
				progress.awardCriteria(criteria);
			}
		}
	}
	
	public void grantTech(Settlement settlement, TechnologyType tech)
	{
		if(plugin.isSQL())
		{
			try {
				PreparedStatement statement = sql.prepareStatement("INSERT INTO Tech (settlement, tech) VALUES (?, ?)");
				statement.setString(1, settlement.getName());
				statement.setString(2, tech.toString());
				statement.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			List<TechnologyType> types = settlement.getTech();
			types.add(tech);
			
			File save = new File(plugin.getDataFolder() + "/settlements/" + settlement.getName() + ".settlement");
			FileConfiguration yaml = YamlConfiguration.loadConfiguration(save);
			yaml.set("tech", types);
			reloadConfiguration(save);
		}
		
		for(OfflinePlayer op : settlement.getMembers())
		{
			if(op.isOnline())
			{
				Player player = Bukkit.getPlayer(op.getUniqueId());
				grantTech(tech.getName(), player);
			}
		}
	}
	
	public void revokeTech(Settlement settlement, TechnologyType tech)
	{
		if(plugin.isSQL())
		{
			try {
				PreparedStatement statement = sql.prepareStatement("DELETE FROM Tech WHERE settlement = ? AND tech = ?");
				statement.setString(1, settlement.getName());
				statement.setString(2, tech.toString());
				statement.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			List<TechnologyType> types = settlement.getTech();
			types.remove(tech);
			
			File save = new File(plugin.getDataFolder() + "/settlements/" + settlement.getName() + ".settlement");
			FileConfiguration yaml = YamlConfiguration.loadConfiguration(save);
			yaml.set("tech", types);
			reloadConfiguration(save);
		}
		
		for(OfflinePlayer op : settlement.getMembers())
		{
			if(op.isOnline())
			{
				Player player = Bukkit.getPlayer(op.getUniqueId());
				revokeTech(tech, player);
			}
		}
	}
	
	/**
	 * Show toast information to a player.
	 * @param name - advancement name
	 * @param player - Bukkit player
	 */
	public void showInfo(String name, Player player)
	{
		AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_INFO + name)));
		progress.awardCriteria("impossible");
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> progress.revokeCriteria("impossible"), 60);
	}
	
	public void showInfo(String name, Settlement settlement)
	{
		for(OfflinePlayer op : settlement.getMembers())
		{
			if(op.isOnline())
			{
				Player player = Bukkit.getPlayer(op.getUniqueId());
				AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_INFO + name)));
				progress.awardCriteria("impossible");
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> progress.revokeCriteria("impossible"), 60);
			}
		}
	}
	
	/**
	 * Check if a player has technology.
	 * @param name - advancement name
	 * @param player - Bukkit player
	 * @return true if player has researched technology
	 */
	public boolean hasTech(String name, Player player)
	{
		AdvancementProgress progress = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, PREFIX_TECH + name)));
		return progress.isDone();
	}
	
	private void reloadConfiguration(File file)
	{
		FileConfiguration yaml = YamlConfiguration.loadConfiguration(file);
		try {
			yaml.save(file);
			yaml.load(file);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}
