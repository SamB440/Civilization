package net.islandearth.civilization.tech;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import lombok.Getter;

public enum TechnologyType {
	ANIMAL_HUSBANDRY("animalhusbandry", "Animal Husbandry", Material.SHEARS, Arrays.asList(ChatColor.AQUA + "25 Science.", 
			"", 
			ChatColor.GRAY + "... to view a full description,",
			ChatColor.GRAY + "open the technology menu (default key: L).",
			"",
			ChatColor.RED + "3 turns to complete."),
			25),
	WRITING("writing", "Writing", "meet", Material.WRITABLE_BOOK, Arrays.asList(ChatColor.AQUA + "25 Science.", 
			"", 
			ChatColor.GRAY + "... to view a full description,",
			ChatColor.GRAY + "open the technology menu (default key: L).",
			"",
			ChatColor.RED + "3 turns to complete."), 50),
	POTTERY("pottery", "Pottery", Material.BRICK, Arrays.asList(ChatColor.AQUA + "25 Science.", 
			"", 
			ChatColor.GRAY + "... to view a full description,",
			ChatColor.GRAY + "open the technology menu (default key: L).",
			"",
			ChatColor.RED + "3 turns to complete."),
			25),
	MINING("mining", "Mining", Material.STONE_PICKAXE, Arrays.asList(ChatColor.AQUA + "25 Science.", 
			"", 
			ChatColor.GRAY + "... to view a full description,",
			ChatColor.GRAY + "open the technology menu (default key: L).",
			"",
			ChatColor.RED + "3 turns to complete."),
			25),
	WHEEL("wheel", "Wheel", Material.PISTON_HEAD, Arrays.asList(ChatColor.AQUA + "25 Science.", 
			"", 
			ChatColor.GRAY + "... to view a full description,", 
			ChatColor.GRAY + "open the technology menu (default key: L).",
			"",
			ChatColor.RED + "3 turns to complete."), 80);
	
	/**
	 * @return the advancement name
	 */
	@Getter private String name;
	/**
	 * @return the user friendly name
	 */
	@Getter private String friendlyName;
	/**
	 * @return the critera
	 */
	@Getter private String criteria;
	/**
	 * @return the advancement material type
	 */
	@Getter private Material type;
	/**
	 * @return the item description
	 */
	@Getter private List<String> description;
	/**
	 * @return science needed to research
	 */
	@Getter private int science;
	
	TechnologyType(String name, String friendly, String criteria, Material type, List<String> description, int science)
	{
		this.name = name;
		this.friendlyName = friendly;
		this.criteria = criteria;
		this.type = type;
		this.type = type;
		this.description = description;
	}
	
	TechnologyType(String name, String friendly, Material type, List<String> description, int science)
	{
		this.name = name;
		this.friendlyName = friendly;
		this.type = type;
		this.type = type;
		this.description = description;
	}
	
	public static TechnologyType fromFriendlyName(String value)
	{
		for(TechnologyType types : values())
		{
			if(types.getFriendlyName().equals(value)) return types;
		} return null;
	}
}
