package net.islandearth.civilization.player;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.schematic.Schematic;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.utils.SQLQuery;

public class CivPlayer {
	
	Civilization plugin;
	Player player;
	OfflinePlayer oplayer;
	FileConfiguration configuration;
	Connection sql;
	
	@Getter
	@Setter
	private Schematic building;
	
	@Getter
	@Setter
	private BuildingType buildingType;
	
	/**
	 * @param plugin - The {@link Civilization} plugin.
	 * @param player - The Bukkit player.
	 */
	public CivPlayer(Civilization plugin, Player player, boolean skip)
	{
		this.plugin = plugin;
		this.player = player;
		this.sql = plugin.getSql();
		
		File pf = new File(plugin.getDataFolder() + "/storage/" + player.getUniqueId() + ".yml");
		configuration = YamlConfiguration.loadConfiguration(pf);
		
		if(!skip)
		{
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				if(!plugin.isSQL())
				{
					if(!pf.exists()) {
						try {
							pf.createNewFile();
							
							configuration = YamlConfiguration.loadConfiguration(pf);
							
							configuration.addDefault("settlement", "null");
							configuration.options().copyDefaults(true);
							reloadConfiguration();
						} catch(IOException e) {
							e.printStackTrace();
						}
					}
				} else {
					try {
						PreparedStatement statement = sql.prepareStatement("SELECT uuid FROM PlayerData WHERE uuid = ?");
						statement.setString(1, player.getUniqueId().toString().replace("-", ""));
						ResultSet rs = statement.executeQuery();
						if(!rs.next())
						{
							PreparedStatement statement2 = sql.prepareStatement("INSERT INTO PlayerData (uuid, settlement) VALUES (?, ?)");
							statement2.setString(1, player.getUniqueId().toString().replace("-", ""));
							statement2.setNull(2, Types.OTHER);
							statement2.executeUpdate();
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				
				if(!pf.exists())
				{
					configuration.addDefault("origin.x", player.getLocation().getX());
					configuration.addDefault("origin.y", player.getLocation().getY());
					configuration.addDefault("origin.z", player.getLocation().getZ());
					configuration.addDefault("viewing", false);
					configuration.addDefault("building", false);
					configuration.addDefault("building-name", "null");
				}
			});
		}
	}
	
	/**
	 * @param plugin - The {@link Civilization} plugin.
	 * @param player - The Offline Bukkit player.
	 * <br></br>
	 * Methods not available:
	 * 	{@link #getOrigin()},
	 * 	{@link #setViewing(boolean, Location)}
	 */
	public CivPlayer(Civilization plugin, OfflinePlayer player)
	{
		this.plugin = plugin;
		this.oplayer = player;
		
		File pf = new File(plugin.getDataFolder() + "/storage/" + player.getUniqueId() + ".yml");
		
		configuration = YamlConfiguration.loadConfiguration(pf);
	}
	
	/**
	 * @return The Bukkit player.
	 */
	public Player getBukkitPlayer()
	{
		return player;
	}
	
	/**
	 * @return the settlement the player is in. May be null.
	 */
	public Settlement getSettlement()
	{
		if(plugin.isSQL()) 
		{
			SQLQuery sq = new SQLQuery(plugin);
			UUID uuid = null;
			if(player != null) uuid = player.getUniqueId();
			else if(oplayer != null) uuid = oplayer.getUniqueId();
			String s = sq.getString("settlement", "PlayerData", uuid);
			if(s != null) return plugin.getCache().getSettlement(s);
			else return null;
		} return null;
	}
	
	public boolean isBuilding()
	{
		return building != null;
	}
	
	public boolean isViewing()
	{
		return configuration.getBoolean("viewing");
	}
	
	public void setViewing(boolean val, Location origin)
	{
		configuration.set("viewing", val);
		configuration.set("origin.x", origin.getX());
		configuration.set("origin.y", origin.getY());
		configuration.set("origin.z", origin.getZ());
		reloadConfiguration();
	}
	
	public Location getOrigin()
	{
		return new Location(getBukkitPlayer().getWorld(), configuration.getDouble("origin.x"), configuration.getDouble("origin.y"), configuration.getDouble("origin.z"));
	}
	
	public void setSettlement(Settlement settlement)
	{
		UUID uuid;
		if(player != null) uuid = player.getUniqueId();
		else uuid = oplayer.getUniqueId();
		if(plugin.isSQL()) 
		{
			SQLQuery sq = new SQLQuery(plugin);
			if(settlement != null) sq.set("settlement", "PlayerData", settlement.getName(), uuid);
			else sq.setNull("settlement", "PlayerData", uuid);
		} else {
			if(settlement != null) configuration.set("settlement", settlement.getName());
			else configuration.set("settlement", "null");
			reloadConfiguration();
		}
	}
	
	/**
	 * Force update a player's stats.
	 */
	public void update(boolean skip)
	{
		File pf = new File(plugin.getDataFolder() + "/storage/" + player.getUniqueId() + ".yml");
		configuration = YamlConfiguration.loadConfiguration(pf);
		
		if(!skip)
		{
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				if(!plugin.isSQL())
				{
					if(!pf.exists()) {
						try {
							pf.createNewFile();
							
							configuration = YamlConfiguration.loadConfiguration(pf);
							
							configuration.addDefault("settlement", "null");
							configuration.options().copyDefaults(true);
							reloadConfiguration();
						} catch(IOException e) {
							e.printStackTrace();
						}
					}
				} else {
					try {
						PreparedStatement statement = sql.prepareStatement("SELECT uuid FROM PlayerData WHERE uuid = ?");
						statement.setString(1, player.getUniqueId().toString().replace("-", ""));
						ResultSet rs = statement.executeQuery();
						if(!rs.next())
						{
							PreparedStatement statement2 = sql.prepareStatement("INSERT INTO PlayerData (uuid, settlement) VALUES (?, ?)");
							statement2.setString(1, player.getUniqueId().toString().replace("-", ""));
							statement2.setNull(2, Types.OTHER);
							statement2.executeUpdate();
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				
				if(!pf.exists())
				{
					configuration.addDefault("origin.x", player.getLocation().getX());
					configuration.addDefault("origin.y", player.getLocation().getY());
					configuration.addDefault("origin.z", player.getLocation().getZ());
					configuration.addDefault("viewing", false);
					configuration.addDefault("building", false);
					configuration.addDefault("building-name", "null");
				}
			});
		}
	}
	
	public void sendTitle(String string, int fadeIn, int stay, int fadeOut) {
		Bukkit.getScheduler().runTask(plugin, () -> player.sendTitle("", string, fadeIn, stay, fadeOut));
	}
	
	public void sendFullTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		Bukkit.getScheduler().runTask(plugin, () -> player.sendTitle(title, subtitle, fadeIn, stay, fadeOut));
	}
	
	public void sendMessage(String string) {
		Bukkit.getScheduler().runTask(plugin, () -> player.sendMessage(string));
	}
	
	public void updateInfoBar() {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Settlement settlement = getSettlement();
			if (settlement != null) {
				if (plugin.getBossbars().containsKey(player.getUniqueId())) {
					BossBar current = plugin.getBossbars().get(player.getUniqueId());
					current.setTitle(ChatColor.AQUA + "⚛" + settlement.getScience() + "  " + ChatColor.LIGHT_PURPLE + "♫" + settlement.getCulture() + "  " + ChatColor.GOLD + "⛁" + settlement.getGold() + 
							"      "
							+ ChatColor.WHITE + "TURN " + settlement.getTurn() + " | " + settlement.getYear() + " " + settlement.getCirca());
				} else {
					BossBar bossbar = Bukkit.createBossBar(ChatColor.AQUA + "⚛" + settlement.getScience() + "  " + ChatColor.LIGHT_PURPLE + "♫" + settlement.getCulture() + "  " + ChatColor.GOLD + "⛁" + settlement.getGold() + 
							"      "
							+ ChatColor.WHITE + "TURN " + settlement.getTurn() + " | " + settlement.getYear() + " " + settlement.getCirca(), 
							BarColor.WHITE, 
							BarStyle.SOLID,
							new BarFlag[0]);
					bossbar.addPlayer(player);
					plugin.getBossbars().put(player.getUniqueId(), bossbar);
				}
			} else {
				if (plugin.getBossbars().containsKey(player.getUniqueId())) {
					plugin.getBossbars().get(player.getUniqueId()).removePlayer(player);
					plugin.getBossbars().remove(player.getUniqueId());
				}
			}
		});
	}
	
	private void reloadConfiguration() {
		File pf = new File(plugin.getDataFolder() + "/storage/" + player.getUniqueId() + ".yml");
		try {
			configuration.save(pf);
			configuration.load(pf);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}
