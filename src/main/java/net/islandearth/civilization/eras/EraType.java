package net.islandearth.civilization.eras;

public enum EraType {
	ANCIENT, 
	CLASSICAL, 
	MEDIEVAL, 
	RENAISSANCE, 
	INDUSTRIAL, 
	MODERN, 
	ATOMIC, 
	INFORMATION
}
