package net.islandearth.civilization.districts;

import lombok.Getter;

public enum DistrictType {
	CITY_CENTRE(0),
	CAMPUS(60),
	COMMERCIAL_HUB(60),
	ENCAMPMENT(60),
	HARBOUR(60),
	HOLY_SITE(60),
	INDUSTRIAL_ZONE(60),
	THEATRE_SQUARE(60),
	AQUEDUCT(50),
	ENTERTAINMENT_COMPLEX(60),
	NEIGHBORHOOD(60),
	AERODOME(60),
	SPACEPORT(2000);
	
	@Getter
	private int cost;
	
	private DistrictType(int cost) {
		this.cost = cost;
	}
}
