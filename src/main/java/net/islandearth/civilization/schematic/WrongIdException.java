package net.islandearth.civilization.schematic;

public class WrongIdException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4472556003462521095L;

	public WrongIdException(String message) {
		super(message);
	}
}
