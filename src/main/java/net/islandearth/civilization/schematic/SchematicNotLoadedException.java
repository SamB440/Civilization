package net.islandearth.civilization.schematic;

public class SchematicNotLoadedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8805283716076251297L;
	
	public SchematicNotLoadedException(String message) {
		super(message);
	}
}
