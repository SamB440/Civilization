package net.islandearth.civilization.settlements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.districts.DistrictType;
import net.islandearth.civilization.eras.EraType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.tech.TechTree;
import net.islandearth.civilization.tech.TechnologyType;
import net.islandearth.civilization.utils.SQLQuery;

public class Settlement {
	
	@Getter private String name;
	@Getter private String world;
	@Getter private int science;
	@Getter private int gold;
	@Getter private int culture;
	@Getter private int turn;
	@Getter private long year;
	@Getter private int boosters;
	@Getter private String circa;
	@Getter private OfflinePlayer owner;
	@Getter private List<OfflinePlayer> members;
	@Getter private List<Building> buildings;
	@Getter private List<DistrictType> districts;
	@Getter private List<TechnologyType> tech;
	@Getter private List<TechnologyType> progresses;
	@Getter private int level;
	@Getter private EraType era;
	@Getter private List<SettlementClaim> claims;
	
	private Civilization plugin;
	private Connection sql;
	
	public Settlement(String name,
			String world,
			int science, 
			int gold, 
			int culture, 
			int turn, 
			long year, 
			int boosters, 
			String circa, 
			OfflinePlayer owner, 
			List<OfflinePlayer> members, 
			List<Building> buildings, 
			List<DistrictType> districts, 
			List<TechnologyType> tech, 
			List<TechnologyType> progresses, 
			int level, 
			EraType era, 
			List<SettlementClaim> claims,
			Civilization plugin) {
		this.name = name;
		this.gold = gold;
		this.culture = culture;
		this.turn = turn;
		this.year = year;
		this.boosters = boosters;
		this.circa = circa;
		this.owner = owner;
		this.members = members;
		this.buildings = buildings;
		this.districts = districts;
		this.tech = tech;
		this.progresses = progresses;
		this.level = level;
		this.era = era;
		this.claims = claims;
		this.plugin = plugin;
		this.sql = plugin.getSql();
	}
	
	/*@SuppressWarnings("deprecation")
	public void create()
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("INSERT INTO Settlements (king, name, level, gold, turn, year, circa, boosters) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
					statement.setString(1, king.getUniqueId().toString().replace("-", ""));
					statement.setString(2, name);
					statement.setInt(3, 1);
					statement.setInt(4, 10);
					statement.setInt(5, 1);
					statement.setInt(6, 4000);
					statement.setString(7, "BC");
					statement.setInt(8, 0);
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				try {
					file.createNewFile();
					yaml.options().copyDefaults(true);
					yaml.addDefault("king", king.getUniqueId().toString());
					yaml.addDefault("level", 1);
					yaml.addDefault("science", 100);
					yaml.addDefault("gold", 10);
					yaml.addDefault("turn", 1);
					yaml.addDefault("year", 4000);
					yaml.addDefault("circa", "BC");
					yaml.addDefault("boosters", 0);
					yaml.addDefault("tech", Arrays.asList(""));
					yaml.addDefault("members", Arrays.asList(king.getUniqueId().toString()));
					yaml.save(file);
					yaml.load(file);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InvalidConfigurationException e) {
					e.printStackTrace();
				}
			}
		});
		
		CivPlayer civ = new CivPlayer(plugin, king, false);
		civ.setSettlement(this);
		
		SchematicReplacer sr = new SchematicReplacer(plugin, new File(plugin.getDataFolder() + "/schematics/tent.schematic"));
		
		List<Location> locations = sr.pasteSchematic(king.getLocation(), king, 10);
		
		Scheduler scheduler = new Scheduler();
		
		scheduler.setTask(Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, () -> {
			for(Location loc : locations)
			{
				if(loc.getBlock().getType() == Material.AIR) Bukkit.getScheduler().runTask(plugin, () -> loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.getX() + 0.5, loc.getY(), loc.getZ() + 0.5, 2));
			}
			
			if(locations.get(locations.size() - 1).getBlock().getType() != Material.AIR || sr.isPasted()) 
			{
				scheduler.cancel();
				
				List<Location> banners = new ArrayList<Location>();
				for(Location location : locations)
				{
					List<Material> whitelist = Arrays.asList(Material.BANNER,
							Material.STANDING_BANNER,
							Material.WALL_BANNER);
					
					if(whitelist.contains(location.getBlock().getType()))
					{
						banners.add(location);
					}
				}
				
				for(Location location : banners)
				{
					int x = location.getBlockX();
					int y = location.getBlockY();
					int z = location.getBlockZ();
					World world = location.getWorld();
					Building building = new Building(plugin, BuildingType.TENT, new Location(world, x, y, z), 1);
					civ.getSettlement().addBuilding(building);
				}
				
				civ.sendFullTitle("Turn 1", "4000 BC", 40, 100, 40);
				
				civ.sendMessage(ChatColor.GREEN + "Right click your tent banner to add science to your Civilization, by inputting diamonds and emeralds.");
				
				sr.setPasted(false);
			}
		}, 0, 40));
			
		king.sendMessage(ChatColor.GREEN + "Tent is now being built!");
		
		TechTree tech = new TechTree(plugin);
		tech.grantTech(Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/root")), king);
		tech.showInfo("root", king);
	}*/
	
	/*public void delete()
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			for(OfflinePlayer players : getMembers())
			{
				if(players.isOnline())
				{
					Player player = Bukkit.getPlayer(players.getUniqueId());
					CivPlayer civ = new CivPlayer(plugin, player, false);
					
					TechTree tree = new TechTree(plugin);
					for(TechnologyType types : civ.getSettlement().getTech())
					{
						tree.revokeTech(civ.getSettlement(), types);
					}
					
					civ.setSettlement(null);
					player.sendMessage(ChatColor.RED + "Your settlement has been disbanded!");
					player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_DEATH, 0.5f, 1f);
					
				} else {
					CivPlayer civ = new CivPlayer(plugin, players);
					civ.setSettlement(null);
				}
			}
			
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("DELETE FROM Settlements WHERE king = ?");
					statement.setString(1, king.getUniqueId().toString().replace("-", ""));
					statement.executeUpdate();
					
					for(Building building : getBuildings())
					{
						removeBuilding(building);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else file.delete();
		});
	}*/
	
	public void exile(OfflinePlayer player)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				SQLQuery sq = new SQLQuery(plugin);
				sq.setNull("settlement", "PlayerData", player.getUniqueId());
				
				members.remove(player);
			}
		});
	}
	
	public void exile(Player player)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
			civ.setSettlement(null);
			
			members.remove(Bukkit.getOfflinePlayer(player.getUniqueId()));
			
			TechTree tree = new TechTree(plugin);
			for(TechnologyType types : TechnologyType.values())
			{
				tree.revokeTech(types, player);
			}
		});
	}
	
	public void setScience(int science)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET science = ? WHERE name = ?");
					statement.setInt(1, science);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.science = science;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public void setGold(int gold)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET gold = ? WHERE name = ?");
					statement.setInt(1, gold);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.gold = gold;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public void setCulture(int culture)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET culture = ? WHERE name = ?");
					statement.setInt(1, culture);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.culture = culture;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public void setTurn(int turn)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET turn = ? WHERE name = ?");
					statement.setInt(1, turn);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.turn = turn;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public void setYear(long year)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET year = ? WHERE name = ?");
					statement.setLong(1, year);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.year = year;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public void setBoosters(int boosters)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET boosters = ? WHERE name = ?");
					statement.setLong(1, boosters);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.boosters = boosters;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void setCirca(String circa)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET circa = ? WHERE name = ?");
					statement.setString(1, circa);
					statement.setString(2, name);
					statement.executeUpdate();
					
					this.circa = circa;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			for(OfflinePlayer op : getMembers())
			{
				if(op.isOnline())
				{
					CivPlayer civ = plugin.getCache().getPlayers().get(Bukkit.getPlayer(op.getUniqueId()).getUniqueId());
					civ.updateInfoBar();
				}
			}
		});
	}
	
	public boolean isKing(Player player)
	{
		if(plugin.isSQL())
		{
			try {
				PreparedStatement statement = sql.prepareStatement("SELECT king FROM Settlements WHERE name = ?");
				statement.setString(1, name);
				
				ResultSet rs = statement.executeQuery();
				if(rs.next())
				{
					StringBuilder builder = new StringBuilder(rs.getString("king"));
				    try {
				        builder.insert(20, "-");
				        builder.insert(16, "-");
				        builder.insert(12, "-");
				        builder.insert(8, "-");
				    } catch (StringIndexOutOfBoundsException e) {
				        e.printStackTrace();
				    } if(builder.toString().equals(player.getUniqueId().toString())) return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	public void addBuilding(Building building)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("INSERT INTO Buildings (settlement, building, world, x, y, z, level) VALUES (?, ?, ?, ?, ?, ?, ?)");
					statement.setString(1, name);
					statement.setString(2, building.getType().toString());
					statement.setString(3, building.getLocation().getWorld().getName());
					statement.setInt(4, (int) building.getLocation().getX());
					statement.setInt(5, (int) building.getLocation().getY());
					statement.setInt(6, (int) building.getLocation().getZ());
					statement.setInt(7, building.getLevel());
					statement.executeUpdate();
					
					buildings.add(building);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				//do stuff
			}
		});
	}
	
	public void removeBuilding(Building building)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("DELETE FROM Buildings WHERE settlement = ? AND building = ? AND world = ? AND x = ? AND y = ? AND z = ? AND level = ?");
					statement.setString(1, name);
					statement.setString(2, building.getType().toString());
					statement.setString(3, building.getLocation().getWorld().getName());
					statement.setInt(4, (int) building.getLocation().getX());
					statement.setInt(5, (int) building.getLocation().getY());
					statement.setInt(6, (int) building.getLocation().getZ());
					statement.setInt(7, building.getLevel());
					statement.executeUpdate();
					
					buildings.remove(building);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				//do stuff
			}
		});
	}
	
	/**
	 * @deprecated this is outdated and does not show granted technology to online players.
	 * @see {@link TechTree}
	 * @param tech - technology type
	 */
	@Deprecated
	public void addTech(TechnologyType tech)
	{
		if(plugin.isSQL())
		{
			try {
				PreparedStatement statement = sql.prepareStatement("INSERT INTO Tech (settlement, tech) VALUES (?, ?)");
				statement.setString(1, name);
				statement.setString(2, tech.toString());
				statement.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean hasTech(TechnologyType tech) {
		if (getTech() != null) {
			return getTech().contains(tech);
		} else {
			return false;
		}
	}
	
	/*public List<SettlementClaim> getClaims(World world)
	{
		List<SettlementClaim> claims = new ArrayList<SettlementClaim>();
		for(File file : new File(plugin.getDataFolder() + "/claims/").listFiles())
		{
			FileConfiguration owner = YamlConfiguration.loadConfiguration(file);
			String[] split = file.getName().replace(".claim", "").replace(world.getName() + ",", "").trim().split(",");
			int x = Integer.valueOf(split[0]);
			int z = Integer.valueOf(split[1]);
			if(owner.getString("Owner").equals(getName())) claims.add(new SettlementClaim(plugin, world.getChunkAt(x, z), this));
		} return claims;
	}*/
	
	public void setLevel(int level)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(plugin.isSQL())
			{
				try {
					PreparedStatement statement = sql.prepareStatement("UPDATE Settlements SET level = ?");
					statement.setInt(1, level);
					statement.executeUpdate();
					
					this.level = level;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * @return all banners associated with this building type for a settlement
	 */
	public List<Location> getAllBanners(BuildingType type)
	{
		if(plugin.isSQL())
		{
			List<Location> buildings = new ArrayList<Location>();
			try {
				PreparedStatement statement = sql.prepareStatement("SELECT world, x, y, z FROM Buildings WHERE settlement = ? AND building = ?");
				statement.setString(1, name);
				statement.setString(2, type.toString());
				
				ResultSet rs = statement.executeQuery();
				while(rs.next()) 
				{
					buildings.add(new Location(Bukkit.getWorld(rs.getString("world")), rs.getInt("x"), rs.getInt("y"), rs.getInt("z")));
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} return buildings;
		} else {
			return null;
		}
	}

}
