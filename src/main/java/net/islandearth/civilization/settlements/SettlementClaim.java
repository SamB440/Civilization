package net.islandearth.civilization.settlements;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Chunk;
import org.bukkit.configuration.file.YamlConfiguration;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.districts.DistrictType;

public class SettlementClaim {
	
	Civilization plugin;
	Chunk chunk;
	String settlement;
	File claim;
	YamlConfiguration yaml;
	
	public SettlementClaim(Civilization plugin, Chunk chunk, String settlement)
	{
		this.plugin = plugin;
		this.chunk = chunk;
		this.settlement = settlement;
		this.claim = new File(plugin.getDataFolder() + "/claims/" + chunk.getWorld().getName() + "," + chunk.getX() + "," + chunk.getZ() + ".claim");
		this.yaml = YamlConfiguration.loadConfiguration(claim);
	}
	
	public SettlementClaim(Civilization plugin, Chunk chunk)
	{
		this.plugin = plugin;
		this.chunk = chunk;
		this.claim = new File(plugin.getDataFolder() + "/claims/" + chunk.getWorld().getName() + "," + chunk.getX() + "," + chunk.getZ() + ".claim");
		this.yaml = YamlConfiguration.loadConfiguration(claim);
	}
	
	/**
	 * @return true if claim was successful, false if claim already existed
	 */
	public boolean claim()
	{
		if(!claim.exists())
		{
			try {
				claim.createNewFile();
				yaml.addDefault("Owner", settlement);
				yaml.addDefault("claimed", true);
				reloadConfiguration(true);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(isRuins()) {
			yaml.set("Owner", settlement);
			setRuins(false);
			reloadConfiguration(false);
			return true;
		} return false;
	}
	
	public void removeOwner()
	{
		yaml.set("claimed", false);
		reloadConfiguration(false);
	}
	
	public boolean isClaimed()
	{
		return yaml.getBoolean("claimed");
	}
	
	public boolean isWilds()
	{
		return claim.exists();
	}
	
	public void remove()
	{
		claim.delete();
	}
	
	public void setRuins(boolean ruins)
	{
		yaml.set("ruins.isruins", ruins);
		yaml.set("claimed", !ruins);
		reloadConfiguration(false);
	}
	
	public boolean isRuins()
	{
		return yaml.getBoolean("ruins.isruins");
	}
	
	public void setDistrict(DistrictType type)
	{
		yaml.set("district", type.toString());
		reloadConfiguration(false);
	}
	
	public DistrictType getDistrict()
	{
		if(EnumUtils.isValidEnum(DistrictType.class, yaml.get("district").toString())) return DistrictType.valueOf(yaml.get("district").toString());
		else return null;
	}
	
	/**
	 * @return the current owner of this land. May be null if not claimed, or the owner does not exist.
	 */
	public String getOwner()
	{
		if(isClaimed()) return yaml.getString("Owner");
		else return null;
	}
	
	public String getRuinsOwner()
	{
		return yaml.getString("Owner");
	}
	
	public Chunk getChunk()
	{
		return chunk;
	}
	
	public String getSettlement()
	{
		return settlement;
	}
	
	public void reloadConfiguration(boolean def)
	{
		try {
			if(def) yaml.options().copyDefaults(true);
			yaml.save(claim);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
