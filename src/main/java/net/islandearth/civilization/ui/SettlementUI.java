package net.islandearth.civilization.ui;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SettlementUI extends UI {

	public SettlementUI() {
		super(27, "Settlement");
		
		for (int i = 0; i < 27; i++) {
			ItemStack blank = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
			setItem(i, blank);
		}
		
		ItemStack info = new ItemStack(Material.SIGN);
		ItemMeta im = info.getItemMeta();
		
		setItem(14, info, player -> {
			
		});
	}
}
