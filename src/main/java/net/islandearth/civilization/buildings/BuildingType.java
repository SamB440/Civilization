package net.islandearth.civilization.buildings;

import org.bukkit.Material;

import lombok.Getter;
import net.islandearth.civilization.districts.DistrictType;

public enum BuildingType {
	FORGE(Material.FURNACE, "Forge", DistrictType.INDUSTRIAL_ZONE), 
	TENT(null, "Tent", DistrictType.CITY_CENTRE),
	MONUMENT(Material.BRICK, "Monument", DistrictType.CITY_CENTRE, 2);
	
	@Getter Material material;
	@Getter String name;
	@Getter DistrictType district;
	@Getter int culture = 0;
	
	BuildingType(Material material, String name, DistrictType district)
	{
		this.material = material;
		this.name = name;
		this.district = district;
	}
	
	BuildingType(Material material, String name, DistrictType district, int culture)
	{
		this.material = material;
		this.name = name;
		this.district = district;
		this.culture = culture;
	}
}
