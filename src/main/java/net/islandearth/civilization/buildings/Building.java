package net.islandearth.civilization.buildings;

import org.bukkit.Location;

import lombok.Getter;
import net.islandearth.civilization.Civilization;

public class Building {
	
	@Getter private Civilization plugin;
	@Getter private BuildingType type;
	@Getter private Location location;
	@Getter private int level;
	
	public Building(Civilization plugin, BuildingType type, Location location, int level)
	{
		this.plugin = plugin;
		this.type = type;
		this.location = location;
		this.level = level;
	}
}
