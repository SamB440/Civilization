package net.islandearth.civilization.leaders;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class LeaderBonus {
	
	@Getter
	@Setter
	private String name;
}
