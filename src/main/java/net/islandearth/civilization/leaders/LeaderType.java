package net.islandearth.civilization.leaders;

public enum LeaderType {
	TEDDY_ROOSEVELT,
	SALADIN,
	MONTEZUMA,
	PEDRO_II,
	QIN_SHI_HUANG,
	CLEOPATRA,
	VICTORIA,
	CATHERINE_DE_MEDICI,
	FREDERICK_BARBAROSSA,
	GORGO,
	PERICLES,
	GANDHI,
	HOJO_TOKIMUNE,
	MVEMBA_A_NZINGA,
	HARALD_HARDRADA,
	CYRUS_II,
	JADWIGA,
	TRAJAN,
	PETER,
	TOMYRIS,
	PHILIP_II,
	GILGAMESH
}
