package net.islandearth.civilization.leaders;

public interface Leader {

	public LeaderType getLeader();
	
	public LeaderBonus getBonuses();
}
