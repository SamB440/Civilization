package net.islandearth.civilization.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.districts.DistrictType;
import net.islandearth.civilization.eras.EraType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.tech.TechTree;
import net.islandearth.civilization.tech.TechnologyType;

public class Cache {
	
	private Civilization plugin;
	
	@Getter
	private HashMap<String, Settlement> settlements = new HashMap<String, Settlement>();
	
	@Getter
	private Map<UUID, CivPlayer> players = new HashMap<>();
	
	public Cache(Civilization plugin)
	{
		this.plugin = plugin;
	}
	
	public Settlement getSettlement(String name)
	{
		return settlements.get(name);
	}
	
	public void addSettlement(Settlement settlement)
	{
		settlements.put(settlement.getName(), settlement);
	}
	
	public void removeSettlement(Settlement settlement)
	{
		settlements.remove(settlement.getName());
	}

	public Settlement createSettlement(Player king, String name, List<SettlementClaim> claims)
	{
		Settlement settlement = new Settlement(name, 
	    		king.getWorld().getName(), 
	    		1,
	    		10,
	    		1,
	    		1,
	    		4000,
	    		0,
	    		"BC",
	    		king,
	    		Arrays.asList(king),
	    		new ArrayList<Building>(),
	    		new ArrayList<DistrictType>(),
	    		new ArrayList<TechnologyType>(),
	    		new ArrayList<TechnologyType>(),
	    		1,
	    		EraType.ANCIENT,
	    		claims, plugin);
		
		try {
			PreparedStatement statement = plugin.getSql().prepareStatement("INSERT INTO Settlements (king, name, world, level, gold, turn, year, circa, boosters) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			statement.setString(1, king.getUniqueId().toString().replace("-", ""));
			statement.setString(2, name);
			statement.setString(3, king.getWorld().getName());
			statement.setInt(4, 1);
			statement.setInt(5, 10);
			statement.setInt(6, 1);
			statement.setInt(7, 4000);
			statement.setString(8, "BC");
			statement.setInt(9, 0);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		settlements.put(name, settlement);
		Bukkit.getScheduler().runTaskLater(plugin, () -> plugin.getCache().getPlayers().get(king.getUniqueId()).updateInfoBar(), 20L);
		return settlement;
		
	}
	
	public void deleteSettlement(Settlement settlement)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			for(OfflinePlayer players : settlement.getMembers())
			{
				if(players.isOnline())
				{
					Player player = Bukkit.getPlayer(players.getUniqueId());
					CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
					
					TechTree tree = new TechTree(plugin);
					if (civ.getSettlement().getTech() != null) {
						for(TechnologyType types : civ.getSettlement().getTech())
						{
							tree.revokeTech(civ.getSettlement(), types);
						}
					}
					
					civ.setSettlement(null);
					Bukkit.getScheduler().runTaskLater(plugin, () -> civ.updateInfoBar(), 20L);
					player.sendMessage(ChatColor.RED + "Your settlement has been disbanded!");
					player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_DEATH, 0.5f, 1f);
					
				} else {
					CivPlayer civ = plugin.getCache().getPlayers().get(players.getUniqueId());
					civ.setSettlement(null);
				}
			}
			
			try {
				PreparedStatement statement = plugin.getSql().prepareStatement("DELETE FROM Settlements WHERE king = ?");
				statement.setString(1, settlement.getOwner().getUniqueId().toString().replace("-", ""));
				statement.executeUpdate();
				
				if (settlement.getBuildings() != null) {
					for(Building building : settlement.getBuildings())
					{
						settlement.removeBuilding(building);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});

	}
}
