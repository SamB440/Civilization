package net.islandearth.civilization.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.tech.TechnologyType;

/**
 * Utils class to retrieve MySQL data from settlements and users.
 * @author SamB440
 */
public class SQLQuery {
	
	private Civilization plugin;
	private Connection sql;
	
	public SQLQuery(Civilization plugin)
	{
		this.plugin = plugin;
		this.sql = plugin.getSql();
		try {
			if (sql == null || sql.isClosed())  {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					try {
						plugin.openConnection();
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}
				});
			}
		} catch (IllegalArgumentException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getInteger(String column, String database, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT " + column + " FROM " + database + " WHERE uuid = ?");
			statement.setString(1, uuid.toString().replaceAll("-", ""));
			ResultSet rs = statement.executeQuery();
			if(rs.next())
			{
				return rs.getInt(column);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getString(String column, String database, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT " + column + " FROM " + database + " WHERE uuid = ?");
			statement.setString(1, uuid.toString().replaceAll("-", ""));
			ResultSet rs = statement.executeQuery();
			if(rs.next())
			{
				return rs.getString(column);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer getInteger(String column, String database, String rclass, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT " + column + " FROM " + database + " WHERE uuid = ? AND class = ?");
			statement.setString(1, uuid.toString().replaceAll("-", ""));
			statement.setString(2, rclass);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
			{
				return rs.getInt(column);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean hasTech(String settlement, TechnologyType type)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT tech FROM Tech WHERE settlement = ? AND tech = ?");
			statement.setString(1, settlement);
			statement.setString(2, type.toString());
			
			ResultSet rs = statement.executeQuery();
			if(rs.next()) return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} return false;
	}
	
	public List<TechnologyType> getTech(String settlement)
	{
		List<TechnologyType> tech = new ArrayList<TechnologyType>();
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT tech FROM Tech WHERE settlement = ?");
			statement.setString(1, settlement);
			
			ResultSet rs = statement.executeQuery();
			while(rs.next()) tech.add(TechnologyType.valueOf(rs.getString("tech")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		} return tech;
	}
	
	public List<Building> getBuildings(String settlement)
	{
		List<Building> buildings = new ArrayList<Building>();
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT building, world, x, y, z, level FROM Buildings WHERE settlement = ?");
			statement.setString(1, settlement);
			
			ResultSet rs = statement.executeQuery();
			while(rs.next()) 
			{
				buildings.add(new Building(plugin,
						BuildingType.valueOf(rs.getString("building")), 
						new Location(
								Bukkit.getWorld(rs.getString("world")), 
								rs.getInt("x"), 
								rs.getInt("y"), 
								rs.getInt("z")), 
								rs.getInt("level")));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} return buildings;
	}
	
	public List<TechnologyType> getProgresses(String settlement)
	{
		List<TechnologyType> tech = new ArrayList<TechnologyType>();
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT tech FROM Tech WHERE progress = ? AND settlement = ?");
			statement.setInt(1, 1);
			statement.setString(2, settlement);
			
			ResultSet rs = statement.executeQuery();
			while(rs.next()) tech.add(TechnologyType.valueOf(rs.getString("tech")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		} return tech;
	}
	
	public List<OfflinePlayer> getMembers(String settlement)
	{
		List<OfflinePlayer> op = new ArrayList<OfflinePlayer>();
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT uuid FROM PlayerData WHERE settlement = ?");
			statement.setString(1, settlement);
			ResultSet rs = statement.executeQuery();
			while(rs.next())
			{
				StringBuilder builder = new StringBuilder(rs.getString("uuid"));
			    try {
			        builder.insert(20, "-");
			        builder.insert(16, "-");
			        builder.insert(12, "-");
			        builder.insert(8, "-");
			    } catch (StringIndexOutOfBoundsException e) {
			        e.printStackTrace();
			    } op.add(Bukkit.getOfflinePlayer(UUID.fromString(builder.toString())));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} return op;
	}
	
	public boolean settlementExists(String settlement)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT name FROM Settlements WHERE name = ?");
			statement.setString(1, settlement);
			ResultSet rs = statement.executeQuery();
			while(rs.next())
			{
				if(rs.getString("name").equals(settlement)) return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} return false;
	}
	
	public double getDouble(String column, String database, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("SELECT " + column + " FROM " + database + " WHERE uuid = ?");
			statement.setString(1, uuid.toString().replaceAll("-", ""));
			
			ResultSet rs = statement.executeQuery();
			if(rs.next()) return rs.getDouble(column);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0.0;
	}
	
	public void set(String column, String database, Object amount, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("UPDATE " + database + " SET " + column + " = ? WHERE uuid = ?");
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				try {
					statement.setObject(1, amount);
					statement.setString(2, uuid.toString().replaceAll("-", ""));
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void set(String column, String database, String value, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("UPDATE " + database + " SET " + column + " = ? WHERE uuid = ?");
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				try {
					statement.setString(1, value);
					statement.setString(2, uuid.toString().replaceAll("-", ""));
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void set(String column, String database, Integer value, String rclass, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("UPDATE " + database + " SET " + column + " = ? WHERE uuid = ? AND class = ?");
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				try {
					statement.setInt(1, value);
					statement.setString(2, uuid.toString().replaceAll("-", ""));
					statement.setString(3, rclass);
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setNull(String column, String database, UUID uuid)
	{
		try {
			PreparedStatement statement = sql.prepareStatement("UPDATE " + database + " SET " + column + " = ? WHERE uuid = ?");
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				try {
					statement.setNull(1, Types.OTHER);
					statement.setString(2, uuid.toString().replaceAll("-", ""));
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
