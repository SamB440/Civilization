package net.islandearth.civilization.utils;

public interface CallBack<T> {
	
	public void run(T ret);

}
