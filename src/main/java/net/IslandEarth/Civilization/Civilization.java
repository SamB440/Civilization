package net.islandearth.civilization;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.islandearth.civilization.commands.SettlementCommand;
import net.islandearth.civilization.districts.DistrictType;
import net.islandearth.civilization.eras.EraType;
import net.islandearth.civilization.listeners.GameListener;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.tasks.BuildTask;
import net.islandearth.civilization.tasks.ClaimTask;
import net.islandearth.civilization.tasks.YearTask;
import net.islandearth.civilization.utils.Cache;
import net.islandearth.civilization.utils.FileUtils;
import net.islandearth.civilization.utils.SQLQuery;
import net.islandearth.languagy.language.Language;
import net.islandearth.languagy.language.Translator;

public class Civilization extends JavaPlugin {
	
	/*
	 * SQL
	 */
    @Getter private Connection sql;
    private String host, database, username, password;
    private int port;
    
    /*
     * Data cache
     */
    
    @Getter private Cache cache;
    
    /*
     * Class variables
     */
    private String c = "[Civilization] ";
    private Logger log = Bukkit.getLogger();
    
    /*
     * Public class variables
     */
    @Getter private List<Entity> removeOnDisable = new ArrayList<Entity>();
    @Getter private Translator translator;
    @Getter private HashMap<UUID, BossBar> bossbars = new HashMap<UUID, BossBar>();
    
    @Getter private Map<UUID, BuildTask> buildTask;
	
	@Override
	public void onEnable()
	{
		createConfig();
		addFiles();
		createSchematics();
		createTranslator();
		registerListeners();
		registerCommands();
		startTasks();
		startSQL();
	}
	
	@Override
	public void onDisable()
	{
		for(Entity entity : removeOnDisable)
		{
			entity.remove();
		}
		
		if(isSQL())
		{
			try {
				sql.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void createConfig()
	{
		getConfig().options().copyDefaults(true);
		getConfig().addDefault("Server.SQL.Enabled", false);
		getConfig().addDefault("Server.SQL.host", "localhost");
		getConfig().addDefault("Server.SQL.port", 3306);
		getConfig().addDefault("Server.SQL.database", "Civilization");
		getConfig().addDefault("Server.SQL.username", "root");
		getConfig().addDefault("Server.SQL.password", "123");
		getConfig().addDefault("Server.Features.tree-regen", true);
		getConfig().addDefault("Server.Science.Worth.Diamonds", 6);
		getConfig().addDefault("Server.Science.Worth.Emeralds", 10);
		getConfig().addDefault("Server.Worlds.Enabled", Arrays.asList("world_dregora"));
		saveConfig();
	}
	
	private void createTranslator() {
		File fallback = new File(getDataFolder() + "/lang/" + "en_gb.yml");
		if (!fallback.exists()) {
			try {
				fallback.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}	
		}
		
		for (Language language : Language.values()) {
			File file = new File(getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			config.options().copyDefaults(true);
			config.addDefault("opening-ui", "&aOpening GUI...");
			config.addDefault("cannot-break", "&cPowerful magic prevents this block from being broken.");
			config.addDefault("cannot-place", "&cPowerful magic prevents this block from being placed.");
			config.addDefault("cannot-interact", "&cPowerful magic prevents this block from being changed.");
			config.addDefault("ruins-regen", "&cThis place is protected by powerful magic, and so it will regenerate over time.");
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		
		this.translator = new Translator(this, fallback);
	}
	
	private void createSchematics()
	{
		File schem1 = new File(getDataFolder() + "/schematics/forge.schem");
		if(!schem1.exists()) saveResource("schematics/forge.schem", true);
		File schem2 = new File(getDataFolder() + "/schematics/camp.schematic");
		if(!schem2.exists()) saveResource("schematics/camp.schematic", true);
		File schem3 = new File(getDataFolder() + "/schematics/tent.schematic");
		if(!schem3.exists()) saveResource("schematics/tent.schematic", true);
		File schem4 = new File(getDataFolder() + "/schematics/monument.schematic");
		if(!schem4.exists()) saveResource("schematics/monument.schematic", true);
	}
	
	private void startSQL()
	{
		if(isSQL())
		{
			log.info(c + "Starting MySQL...");
	        host = getConfig().getString("Server.SQL.host");
	        port = getConfig().getInt("Server.SQL.port");
	        database = getConfig().getString("Server.SQL.database") + "?useSSL=false" + "&autoReconnect=true";
	        username = getConfig().getString("Server.SQL.username");
	        password = getConfig().getString("Server.SQL.password");
	        try {
				openConnection();
				
				PreparedStatement statement = sql.prepareStatement("CREATE TABLE IF NOT EXISTS PlayerData (uuid varchar(36) NOT NULL, settlement varchar(36), PRIMARY KEY (uuid))");
				statement.executeUpdate();
				
				PreparedStatement s3 = sql.prepareStatement("CREATE TABLE IF NOT EXISTS Settlements (king varchar(36) NOT NULL, name varchar(36), world varchar(36), level int, science int, gold int, culture int, turn int, year bigint, circa varchar(2), boosters int, PRIMARY KEY(king, name))");
				s3.executeUpdate();
				
				PreparedStatement s4 = sql.prepareStatement("CREATE TABLE IF NOT EXISTS Tech (settlement varchar(36) NOT NULL, tech varchar(36) NOT NULL, progress int, PRIMARY KEY(settlement, tech))");
				s4.executeUpdate();
				
				PreparedStatement s5 = sql.prepareStatement("CREATE TABLE IF NOT EXISTS Buildings (settlement varchar(36) NOT NULL, building varchar(36) NOT NULL, world varchar(36) NOT NULL, x int NOT NULL, y int NOT NULL, z int NOT NULL, level int NOT NULL, PRIMARY KEY(settlement, building, world, x, y, z))");
				s5.executeUpdate();
				
				PreparedStatement s6 = sql.prepareStatement("CREATE TABLE IF NOT EXISTS Districts (settlement varchar(36) NOT NULL, district varchar(36) NOT NULL, world varchar(36) NOT NULL, x int NOT NULL, z int NOT NULL, PRIMARY KEY(settlement, district, world, x, z))");
				s6.executeUpdate();
				
				statement.close();
				
				cache = new Cache(this);
				
				PreparedStatement s7 = sql.prepareStatement("SELECT * FROM Settlements");
				ResultSet rs = s7.executeQuery();
				while(rs.next())
				{
					StringBuilder builder = new StringBuilder(rs.getString("king"));
				    try {
				        builder.insert(20, "-");
				        builder.insert(16, "-");
				        builder.insert(12, "-");
				        builder.insert(8, "-");
				    } catch (StringIndexOutOfBoundsException e) {
				        e.printStackTrace();
				    }
				    
				    OfflinePlayer king = Bukkit.getOfflinePlayer(UUID.fromString(builder.toString()));
				    String name = rs.getString("name");
				    String w = rs.getString("world");
				    int level = rs.getInt("level");
				    int science = rs.getInt("science");
				    int gold = rs.getInt("gold");
				    int culture = rs.getInt("culture");
				    int turn = rs.getInt("turn");
				    long year = rs.getLong("year");
				    String circa = rs.getString("circa");
				    int boosters = rs.getInt("boosters");
				    
				    SQLQuery sq = new SQLQuery(this);
				    
				    World world = Bukkit.getWorld(w);
					List<SettlementClaim> claims = new ArrayList<SettlementClaim>();
					for(File file : new File(getDataFolder() + "/claims/").listFiles())
					{
						FileConfiguration owner = YamlConfiguration.loadConfiguration(file);
						String[] split = file.getName().replace(".claim", "").replace(world.getName() + ",", "").trim().split(",");
						int x = Integer.valueOf(split[0]);
						int z = Integer.valueOf(split[1]);
						if(owner.getString("Owner").equals(getName())) claims.add(new SettlementClaim(this, world.getChunkAt(x, z), name));
					}
					
					List<DistrictType> districts = new ArrayList<DistrictType>();
					for(SettlementClaim claim : claims)
					{
						if(claim.getDistrict() != null) districts.add(claim.getDistrict());
					}
					
				    cache.addSettlement(new Settlement(name, 
				    		w, 
				    		science,
				    		gold,
				    		culture,
				    		turn,
				    		year,
				    		boosters,
				    		circa,
				    		king,
				    		sq.getMembers(name),
				    		sq.getBuildings(name),
				    		districts,
				    		sq.getTech(name),
				    		sq.getProgresses(name),
				    		level,
				    		EraType.ANCIENT,
				    		claims, this));
				}
			    
			} catch (ClassNotFoundException | SQLException e) {
				log.severe(c + "There was an error whilst starting MySQL as follows;");
				e.printStackTrace();
				Bukkit.getServer().getPluginManager().disablePlugin(this);
			}
		}
	}
	
	public void openConnection() throws SQLException, ClassNotFoundException 
	{
	    if(sql != null && !sql.isClosed()) sql = null;
	 
	    synchronized (this)
	    {
	        Class.forName("com.mysql.jdbc.Driver");
	        sql = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
	    }
	}
	
	private void registerListeners()
	{
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new GameListener(this), this);
	}
	
	private void registerCommands()
	{
		getCommand("Settlement").setExecutor(new SettlementCommand(this));
	}
	
	@SuppressWarnings("deprecation")
	private void startTasks() {
		this.buildTask = new HashMap<UUID, BuildTask>();
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new ClaimTask(this), 0, 40);
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new YearTask(this), 0, 4800);
	}

	private void addFiles()
	{
		File folder = new File("plugins/Civilization/storage/");
		File folder2 = new File("plugins/Civilization/lang/");
		File folder3 = new File("plugins/Civilization/claims/");
		File folder4 = new File("plugins/Civilization/settlements/");
		File folder5 = new File("plugins/Civilization/schematics/");
		if(!folder.exists()) folder.mkdir();
		if(!folder2.exists()) folder2.mkdir();
		if(!folder3.exists()) folder3.mkdir();
		if(!folder4.exists()) folder4.mkdir();
		if(!folder5.exists()) folder5.mkdir();
		
		File zip = new File("plugins/Civilization/datapacks/datapack.zip");
		if (!zip.exists()) saveResource("datapacks/datapack.zip", true);
		
		for (String string : getConfig().getStringList("Server.Worlds.Enabled")) {
			World world = Bukkit.getWorld(string);
			if (world != null) {
				File loc = new File(new File(".").getAbsolutePath() + "/" + world.getName() + "/datapacks/civilization");
				if (!loc.exists()) {
					log.info("Installing datapack for world '" + world.getName() + "'");
					loc.mkdirs();
					try {
						FileUtils.createDatapack(zip.getPath(), new File(new File(".").getAbsolutePath() + "/" + world.getName() + "/datapacks/").getPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public boolean isSQL()
	{
		return getConfig().getBoolean("Server.SQL.Enabled");
	}
}
