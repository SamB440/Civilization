package net.islandearth.civilization.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.AllArgsConstructor;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.schematic.Scheduler;
import net.islandearth.civilization.schematic.Schematic;
import net.islandearth.civilization.schematic.SchematicNotLoadedException;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.tech.TechTree;
import net.islandearth.civilization.tech.TechnologyType;
import net.islandearth.civilization.ui.BannerUI;
import net.islandearth.civilization.ui.ScienceUI;
import net.islandearth.civilization.ui.SettingsUI;
import net.islandearth.civilization.ui.UI;
import net.islandearth.civilization.ui.UI.ItemClick;

@AllArgsConstructor
public class GameListener implements Listener {
	
	private Civilization plugin;
	
	/* --------------
	 * JOIN AND LEAVE
	 */
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
			CivPlayer player = new CivPlayer(plugin, pje.getPlayer(), false);
			plugin.getCache().getPlayers().put(pje.getPlayer().getUniqueId(), player);
			player.updateInfoBar();
			
			if (player.getSettlement() != null) {
				new TechTree(plugin).sync(player);
			} else {
				TechTree tree = new TechTree(plugin);
				for (TechnologyType types : TechnologyType.values()) {
					if(tree.hasTech(types.getName(), player.getBukkitPlayer())) tree.revokeTech(types, player.getBukkitPlayer());
				}
			}
		}, 20);
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent pqe) {
		Player player = pqe.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
			if (civ.isBuilding()) {
				civ.setBuilding(null);
			}
			
			if (UI.getOpen().containsKey(player.getUniqueId())) {
				UI.getOpen().remove(player.getUniqueId());
			}
			
			if (plugin.getBossbars().containsKey(player.getUniqueId())) {
				plugin.getBossbars().remove(player.getUniqueId());
			}
			
			plugin.getCache().getPlayers().remove(player.getUniqueId());
		});
	}
	
	/* --------------
	 * PLACING AND BREAKING
	 */
	
	@EventHandler
	public void onBreak(BlockBreakEvent bbe)
	{
		Player player = bbe.getPlayer();
		CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
		Settlement settlement = civ.getSettlement();
		SettlementClaim sc = new SettlementClaim(plugin, bbe.getBlock().getChunk());
		if(sc.getOwner() != null) 
		{
			if(settlement != null)
			{
				if(!sc.getOwner().equals(settlement.getName())) 
				{
					player.getWorld().spawnParticle(Particle.CLOUD, bbe.getBlock().getX() + 0.5, bbe.getBlock().getY(), bbe.getBlock().getZ() + 0.5, 10);
					civ.sendMessage(getString(player, "cannot-break"));
					bbe.setCancelled(true);
				} else {
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						List<Building> buildings = settlement.getBuildings();
						for (Building building : buildings) {
							if (building.getLocation().equals(bbe.getBlock().getLocation())) {
								settlement.removeBuilding(building);
								civ.sendMessage(ChatColor.RED + "Removed building " + StringUtils.capitalize(building.getType().toString().toLowerCase()) + "!");
							}
						}
					});
				}
			} else {
				player.getWorld().spawnParticle(Particle.CLOUD, bbe.getBlock().getX() + 0.5, bbe.getBlock().getY(), bbe.getBlock().getZ() + 0.5, 10);
				civ.sendMessage(getString(player, "cannot-break"));
				bbe.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent bpe)
	{
		Player player = bpe.getPlayer();
		CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
		SettlementClaim sc = new SettlementClaim(plugin, bpe.getBlock().getChunk());
		if(sc.getOwner() != null) 
		{
			if(civ.getSettlement() != null)
			{
				if(!sc.getOwner().equals(civ.getSettlement().getName())) 
				{
					player.getWorld().spawnParticle(Particle.CLOUD, bpe.getBlock().getX() + 0.5, bpe.getBlock().getY(), bpe.getBlock().getZ() + 0.5, 10);
					player.sendMessage(getString(player, "cannot-place"));
					bpe.setCancelled(true);
				}
			} else {
				player.getWorld().spawnParticle(Particle.CLOUD, bpe.getBlock().getX() + 0.5, bpe.getBlock().getY(), bpe.getBlock().getZ() + 0.5, 10);
				player.sendMessage(getString(player, "cannot-place"));
				bpe.setCancelled(true);
			}
		}
	}
	
	/* --------------
	 * INTERACTING
	 */
	
	@EventHandler
	public void onInteract(PlayerInteractEvent pie)
	{
		if(pie.getHand() == EquipmentSlot.OFF_HAND) return;
		if(pie.getClickedBlock() == null) return;
		
		Player player = pie.getPlayer();
		CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
		Settlement settlement = civ.getSettlement();
		Block block = pie.getClickedBlock();
		SettlementClaim sc = new SettlementClaim(plugin, block.getChunk());
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(sc.isClaimed())
			{
				if(settlement != null)
				{
					if(!sc.getOwner().equals(settlement.getName()))
					{
						Bukkit.getScheduler().runTask(plugin, () -> player.getWorld().spawnParticle(Particle.VILLAGER_ANGRY, block.getX() + 0.5, block.getY(), block.getZ() + 0.5, 2));
						civ.sendMessage(getString(player, "cannot-interact"));
						pie.setCancelled(true);
					} else if(pie.getAction() == Action.RIGHT_CLICK_BLOCK || pie.getAction() == Action.PHYSICAL) {
						List<Building> buildings = settlement.getBuildings();
						for(Building building : buildings)
						{
							if(building.getLocation().equals(block.getLocation()))
							{
								new SettingsUI(plugin, building).openInventory(plugin, player);
							}
						}
					}
				} else {
					Bukkit.getScheduler().runTask(plugin, () -> player.getWorld().spawnParticle(Particle.VILLAGER_ANGRY, block.getX() + 0.5, block.getY(), block.getZ() + 0.5, 2));
					civ.sendMessage(getString(player, "cannot-interact"));
					pie.setCancelled(true);
				}
			}
		});
		
		if(pie.getAction() == Action.RIGHT_CLICK_AIR || pie.getAction() == Action.RIGHT_CLICK_BLOCK) 
		{
			if(settlement != null)
			{
				SettlementClaim target = new SettlementClaim(plugin, player.getTargetBlock(null, 7).getChunk());
				if(civ.isBuilding() && target.getOwner().equals(settlement.getName()) && target.getDistrict() != null)
				{
					//if(target.getDistrict().equals(civ.getBuildingType().getDistrict()))
					//{
						try {
							Schematic schematic = civ.getBuilding();
							List<Location> locations = schematic.loadSchematic().pasteSchematic(player.getTargetBlock(null, 7).getLocation().setDirection(player.getEyeLocation().getDirection()).add(0, 1, 0), player, 5);
							if(locations != null)
							{
								
								if(plugin.getBuildTask().get(player.getUniqueId()).getCache().containsKey(player))
								{
									for(Location clear : plugin.getBuildTask().get(player.getUniqueId()).getCache().get(player))
									{
										clear.getBlock().getState().update(true, false);
									}
								}
								
								final BuildingType type = civ.getBuildingType();
								
								Scheduler scheduler = new Scheduler();
								
								scheduler.setTask(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
									for(Location loc : locations)
									{
										if(loc.getBlock().getType() == Material.AIR) loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.getX() + 0.5, loc.getY(), loc.getZ() + 0.5, 2);
									}
									
									if(locations.get(locations.size() - 1).getBlock().getType() != Material.AIR) 
									{
										scheduler.cancel();
										
										List<Location> banners = new ArrayList<Location>();
										for(Location location : locations)
										{
											Set<Material> whitelist = Tag.BANNERS.getValues();
											
											if(whitelist.contains(location.getBlock().getType()))
											{
												banners.add(location);
											}
										}
										
										int x = (int) banners.get(0).getX();
										int y = (int) banners.get(0).getY();
										int z = (int) banners.get(0).getZ();
										World world = banners.get(0).getWorld();
										Building building = new Building(plugin, type, new Location(world, x, y, z), 1);
										settlement.addBuilding(building);
										
										new BannerUI(plugin, settlement, banners, building).openInventory(plugin, player);
									}
								}, 0, 40));
								
								player.sendMessage(ChatColor.GREEN + "Forge is now being built!");
								civ.setBuilding(null);
							} else {
								player.sendMessage(ChatColor.RED + "You can't build that here!");
							}
						} catch (SchematicNotLoadedException e) {
							e.printStackTrace();
						}
					//} else civ.sendMessage(ChatColor.RED + "You can only place that inside a " + ChatColor.GREEN + StringUtils.capitalize(civ.getBuildingType().getDistrict().toString().toLowerCase().replace("_", " ")) + ChatColor.RED + " district!");
				} else if(civ.isBuilding()) civ.sendMessage(ChatColor.RED + "You can't place that here!");
			}
		} else if(pie.getAction() == Action.LEFT_CLICK_AIR || pie.getAction() == Action.LEFT_CLICK_BLOCK) {
			if(settlement != null)
			{
				if(civ.isBuilding())
				{
					civ.setBuilding(null);
					player.sendMessage(ChatColor.RED + "Cancelled building placement.");
					
					if(plugin.getBuildTask().get(player.getUniqueId()).getCache().containsKey(player))
					{
						for(Location clear : plugin.getBuildTask().get(player.getUniqueId()).getCache().get(player))
						{
							clear.getBlock().getState().update(true, false);
						}
					}
				}
			}
		}
	}
	
	/* --------------
	 * INVENTORY
	 */
	
	@EventHandler
	public void onClick(InventoryClickEvent ice)
	{
		if(ice.getWhoClicked() instanceof Player)
		{
			Player player = (Player) ice.getWhoClicked();
			UUID uuid = UI.getOpen().get(player.getUniqueId());
			if(uuid != null)
			{
				UI ui = UI.getInventories().get(uuid);
				if(!(ui instanceof ScienceUI)) ice.setCancelled(true);
				ItemClick action = ui.getActions().get(ice.getSlot());
				
				if(action != null)
				{
					action.click(player);
				}
			}
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent ice)
	{
		if(ice.getPlayer() instanceof Player)
		{
			Player player = (Player) ice.getPlayer();
			UI ui = UI.getInventories().get(UI.getOpen().get(player.getUniqueId()));
			
			if(ui instanceof ScienceUI)
			{
				ItemStack confirm = new ItemStack(Material.ARROW);
				ItemMeta cm = confirm.getItemMeta();
				cm.setDisplayName(ChatColor.GREEN + "Confirm");
				confirm.setItemMeta(cm);
				
				for(ItemStack items : player.getOpenInventory().getTopInventory().getContents())
				{
					if(items != null && !items.equals(confirm))
					{
						Bukkit.getScheduler().runTask(plugin, () -> player.getWorld().dropItem(player.getEyeLocation(), items));
					}
				}
			}
			
			if(ui != null) ui.delete();
			if(UI.getOpen().containsKey(player.getUniqueId())) UI.getOpen().remove(player.getUniqueId());
		}
	}
	
	/* --------------
	 * UTILS
	 */
	
	private String getString(Player player, String path) {
		return ChatColor.translateAlternateColorCodes('&', plugin.getTranslator().getTranslationFor(player, path));
	}
}
