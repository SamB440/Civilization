package net.islandearth.civilization.ui;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.settlements.Settlement;

public class SettingsUI extends UI {
	
	public SettingsUI(Civilization plugin, Building building)
	{
		super(9, "Settings");
		
		ItemStack close = new ItemStack(Material.BARRIER);
		ItemMeta cm = close.getItemMeta();
		cm.setDisplayName(ChatColor.GREEN + "Close");
		close.setItemMeta(cm);
		
		ItemStack banner = new ItemStack(Material.BLACK_BANNER);
		ItemMeta bm = banner.getItemMeta();
		bm.setDisplayName(ChatColor.WHITE + "Change banner colour");
		banner.setItemMeta(bm);
		
		ItemStack level = new ItemStack(Material.EXPERIENCE_BOTTLE);
		ItemMeta lm = level.getItemMeta();
		lm.setDisplayName(ChatColor.GREEN + "Level: " + building.getLevel());
		lm.setLore(Arrays.asList(ChatColor.WHITE + "Left click to upgrade."));
		level.setItemMeta(lm);
		
		setItem(8, close, player -> {
			player.closeInventory();
		});
		
		setItem(4, level, player -> {
			player.sendMessage("upgrade stuffz");
		});
		
		switch(building.getType())
		{
			case FORGE:
				setItem(7, banner, player -> {
					Settlement settlement = plugin.getCache().getPlayers().get(player.getUniqueId()).getSettlement();
					new BannerUI(plugin, settlement, building).openInventory(plugin, player);
				});
				break;
			case TENT:
				setItem(7, banner, player -> {
					Settlement settlement = plugin.getCache().getPlayers().get(player.getUniqueId()).getSettlement();
					new BannerUI(plugin, settlement, settlement.getAllBanners(BuildingType.TENT), building).openInventory(plugin, player);
				});
				
				ItemStack build = new ItemStack(Material.BRICK);
				ItemMeta bdm = build.getItemMeta();
				bdm.setDisplayName(ChatColor.WHITE + "Build");
				build.setItemMeta(bdm);
				setItem(6, build, player -> {
					new BuildUI(plugin).openInventory(plugin, player);
				});
				
				ItemStack science = new ItemStack(Material.POTION);
				ItemMeta sm = science.getItemMeta();
				sm.setDisplayName(ChatColor.AQUA + "Add Science");
				science.setItemMeta(sm);
				
				setItem(0, science, player -> {
					Settlement settlement = plugin.getCache().getPlayers().get(player.getUniqueId()).getSettlement();
					new ScienceUI(plugin, settlement, building).openInventory(plugin, player);
				});
				
				ItemStack research = new ItemStack(Material.LINGERING_POTION);
				ItemMeta rm = research.getItemMeta();
				rm.setDisplayName(ChatColor.AQUA + "Research");
				research.setItemMeta(rm);
				
				setItem(1, research, player -> {
					Settlement settlement = plugin.getCache().getPlayers().get(player.getUniqueId()).getSettlement();
					new TechUI(plugin, settlement).openInventory(plugin, player);
				});
				
				break;
			default:
				break;
		}
	}
}
