package net.islandearth.civilization.ui;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Banner;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.settlements.Settlement;

public class BannerUI extends UI {
	
	private static int rounded = (int) roundUp(DyeColor.values().length, 9);

	public BannerUI(Civilization plugin, Settlement settlement, List<Location> banners, Building building) {
		super(rounded, "Choose this building's colour");
		
		int current = 0;
		
		for (Material wool : Tag.WOOL.getValues()) {
			ItemStack item = new ItemStack(wool);
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(StringUtils.capitalize(wool.toString().toLowerCase().replace("_", " ").replace("WOOL", "")));
			item.setItemMeta(im);
			
			String colour = wool.toString().replace("_WOOL", "");
			
			setItem(current, item, player -> {
				for (Location location : banners) {
					Banner banner = (Banner) location.getBlock().getState();
					banner.setBaseColor(DyeColor.valueOf(colour));
					banner.update();
					player.closeInventory();
					new SettingsUI(plugin, building).openInventory(plugin, player);
				}
			});
			
			current++;
		}
	}
	
	public BannerUI(Civilization plugin, Settlement settlement, Building building) {
		super(rounded, "Choose this building's colour");
		
		int current = 0;
		
		for (Material wool : Tag.WOOL.getValues()) {
			ItemStack item = new ItemStack(wool);
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(StringUtils.capitalize(wool.toString().toLowerCase().replace("_", " ").replace("WOOL", "")));
			item.setItemMeta(im);
			
			String colour = wool.toString().replace("_WOOL", "");
			
			setItem(current, item, player -> {
				Banner banner = (Banner) building.getLocation().getBlock().getState();
				banner.setBaseColor(DyeColor.valueOf(colour));
				banner.update();
				player.closeInventory();
				new SettingsUI(plugin, building).openInventory(plugin, player);
			});
			current++;
		}
	}

	private static long roundUp(long n, long m) {
	    return n >= 0 ? ((n + m - 1) / m) * m : (n / m) * m;
	}
}
