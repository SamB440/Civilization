package net.islandearth.civilization.ui;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.schematic.Schematic;

public class BuildUI extends UI {

	public BuildUI(Civilization plugin) {
		super((int) roundUp(BuildingType.values().length, 9), "Available Buildings");
		
		int current = 0;
		
		for(BuildingType type : BuildingType.values())
		{
			if(type.getMaterial() != null)
			{
				ItemStack item = new ItemStack(type.getMaterial());
				ItemMeta im = item.getItemMeta();
				im.setDisplayName(ChatColor.WHITE + type.getName());
				item.setItemMeta(im);
				
				setItem(current, item, player -> {
					CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
					civ.setBuilding(new Schematic(plugin, new File(plugin.getDataFolder() + "/schematics/" + type.toString() + ".schem")).loadSchematic());
					civ.setBuildingType(BuildingType.FORGE);
					player.closeInventory();
				});
				
				current++;
			}
		}
	}
	
	private static long roundUp(long n, long m) {
	    return n >= 0 ? ((n + m - 1) / m) * m : (n / m) * m;
	}
}
