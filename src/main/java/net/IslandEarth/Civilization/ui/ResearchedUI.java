package net.islandearth.civilization.ui;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.tech.TechnologyType;

public class ResearchedUI extends UI {
	
	public ResearchedUI(Civilization plugin, Settlement settlement) 
	{
		super((int) roundUp(settlement.getTech().size(), 9), ChatColor.AQUA + "Researched Technology");
		
		List<TechnologyType> tech = settlement.getTech();
		if(!tech.isEmpty())
		{
			int slot = 0;
			for(TechnologyType types : tech)
			{
				ItemStack item = new ItemStack(types.getType());
				ItemMeta im = item.getItemMeta();
				im.setDisplayName(ChatColor.AQUA + types.getFriendlyName());
				im.setLore(Arrays.asList(ChatColor.WHITE + "Already researched!"));
				im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				item.setItemMeta(im);
				
				setItem(slot, item, player -> {
					player.sendMessage(ChatColor.GREEN + "This technology has already been researched!");
				});
				
				slot++;
			}
			
			ItemStack research = new ItemStack(Material.LINGERING_POTION);
			research.addUnsafeEnchantment(Enchantment.BINDING_CURSE, 0);
			ItemMeta rm = research.getItemMeta();
			rm.setDisplayName(ChatColor.AQUA + "Researchable Technology");
			rm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
			research.setItemMeta(rm);
			
			setItem((int) roundUp(settlement.getTech().size(), 9) - 1, research, player -> {
				new TechUI(plugin, settlement).openInventory(plugin, player);
			});
		}
	}
	
	private static long roundUp(long n, long m) 
	{
	    return n >= 0 ? ((n + m - 1) / m) * m : (n / m) * m;
	}
}
