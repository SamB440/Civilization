package net.islandearth.civilization.ui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.settlements.Settlement;

public class ScienceUI extends UI {

	public ScienceUI(Civilization plugin, Settlement settlement, Building building) {
		super(9, ChatColor.AQUA + "Add science");
		
		ItemStack confirm = new ItemStack(Material.ARROW);
		ItemMeta cm = confirm.getItemMeta();
		cm.setDisplayName(ChatColor.GREEN + "Confirm");
		confirm.setItemMeta(cm);
		
		setItem(8, confirm, player -> {
			
			int total = 0;
			for(ItemStack items : player.getOpenInventory().getTopInventory().getContents())
			{
				if(items != null && !items.equals(confirm))
				{
					switch(items.getType())
					{
						case DIAMOND:
							total = getInt(plugin, "Server.Science.Worth.Diamonds") * items.getAmount();
							settlement.setScience(settlement.getScience() + total);
							break;
						case EMERALD:
							total = getInt(plugin, "Server.Science.Worth.Emeralds") * items.getAmount();
							settlement.setScience(settlement.getScience() + total);
							break;
						default:
							Bukkit.getScheduler().runTask(plugin, () -> player.getWorld().dropItem(player.getEyeLocation(), items));
							break;
					}
				}
			}
			
			player.getOpenInventory().getTopInventory().clear();
			player.closeInventory();
			
			player.sendTitle("", ChatColor.AQUA + "+" + total + " Science", 0, 40, 0);
			
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> new SettingsUI(plugin, building).openInventory(plugin, player), 40);
		});
	}
	
	private int getInt(Civilization plugin, String path)
	{
		return plugin.getConfig().getInt(path);
	}
}
