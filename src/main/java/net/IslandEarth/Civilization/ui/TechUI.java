package net.islandearth.civilization.ui;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.tech.Technology;
import net.islandearth.civilization.tech.TechnologyType;

public class TechUI extends UI {
	
	private static int rounded = (int) roundUp(TechnologyType.values().length, 9);
	
	public TechUI(Civilization plugin, Settlement settlement) {
		super(rounded, ChatColor.AQUA + "Researchable Technology");
		
		int slot = 0;
		for(TechnologyType types : TechnologyType.values())
		{
			if(!settlement.hasTech(types))
			{
				ItemStack item = new ItemStack(types.getType());
				ItemMeta im = item.getItemMeta();
				im.setDisplayName(ChatColor.AQUA + types.getFriendlyName());
				im.setLore(types.getDescription());
				im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				item.setItemMeta(im);
				
				setItem(slot, item, player -> {
					Technology tech = new Technology(plugin, plugin.getCache().getPlayers().get(player.getUniqueId()).getSettlement(), player, 3, 200, types);
					tech.startResearch();
					player.closeInventory();
				});
				
				slot++;
			}
		}
		
		if(!settlement.getTech().isEmpty())
		{
			ItemStack researched = new ItemStack(Material.LINGERING_POTION);
			researched.addUnsafeEnchantment(Enchantment.BINDING_CURSE, 0);
			ItemMeta rm = researched.getItemMeta();
			rm.setDisplayName(ChatColor.AQUA + "Researched Technology");
			rm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
			researched.setItemMeta(rm);
			
			setItem(rounded - 1, researched, player -> {
				new ResearchedUI(plugin, settlement).openInventory(plugin, player);
			});
		}
	}

	private static long roundUp(long n, long m) 
	{
	    return n >= 0 ? ((n + m - 1) / m) * m : (n / m) * m;
	}
}
