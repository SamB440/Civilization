package net.islandearth.civilization.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Animals;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.districts.DistrictType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.schematic.Schematic;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.tasks.BuildTask;
import net.islandearth.civilization.tech.TechTree;
import net.islandearth.civilization.tech.Technology;
import net.islandearth.civilization.tech.TechnologyType;
import net.islandearth.civilization.ui.TechUI;
import net.islandearth.civilization.utils.SQLQuery;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

@RequiredArgsConstructor
public class SettlementCommand implements CommandExecutor {
	
	@NonNull
	private Civilization plugin;
	
	private HashMap<Player, Integer> cancel = new HashMap<Player, Integer>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if(args.length == 4)
		{
			switch(args[0].toLowerCase())
			{
				case "admin":
					
					if(sender.isOp() || sender.hasPermission("Civilization.admin"))
					{
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							switch(args[1].toLowerCase())
							{
								case "addscience": {
									CivPlayer civ = plugin.getCache().getPlayers().get(UUID.fromString(args[2]));
									Settlement settlement = civ.getSettlement();
									if(settlement != null)
									{
										settlement.setScience(settlement.getScience() + Integer.valueOf(args[3]));
									} else civ.sendMessage(ChatColor.RED + "Target settlement does not exist!");
									
									break;
									
								}
									
								case "addgold": {
									CivPlayer civ = plugin.getCache().getPlayers().get(UUID.fromString(args[2]));
									Settlement settlement = civ.getSettlement();
									if(settlement != null)
									{
										settlement.setGold(settlement.getGold() + Integer.valueOf(args[3]));
									} else civ.sendMessage(ChatColor.RED + "Target settlement does not exist!");
									
									break;
								}
								
								case "addboosters": {
									CivPlayer civ = plugin.getCache().getPlayers().get(UUID.fromString(args[2]));
									Settlement settlement = civ.getSettlement();
									if(settlement != null)
									{
										settlement.setBoosters(settlement.getBoosters() + Integer.valueOf(args[3]));
									} else civ.sendMessage(ChatColor.RED + "Target settlement does not exist!");
									
									break;
								}
								
								default:
									sendHelp(sender);
							}
						});
					} else sender.sendMessage(ChatColor.RED + "You do not have permission to execute this command!");
				
				break;
				
				default:
					sendHelp(sender);
					break;
			}
		} else {
			
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
				switch(args.length)
				{
					
					case 2:
						
						switch(args[0].toLowerCase())
						{
							case "build":
								if(civ.getSettlement() != null)
								{
									switch(args[1].toLowerCase())
									{
										case "forge":
											if (civ.getSettlement().hasTech(TechnologyType.POTTERY)) {
												civ.setBuilding(new Schematic(plugin, new File(plugin.getDataFolder() + "/schematics/forge.schem")).loadSchematic());
												civ.setBuildingType(BuildingType.FORGE);
												new BuildTask(plugin, player).start();
											} else {
												player.sendMessage(ChatColor.RED + "You need to research pottery to build this!");
											}
											break;
										case "test":
											civ.setBuilding(new Schematic(plugin, new File(plugin.getDataFolder() + "/schematics/test.schem")).loadSchematic());
											civ.setBuildingType(BuildingType.FORGE);
											new BuildTask(plugin, player).start();
											break;
										default:
											player.sendMessage(ChatColor.RED + "That building does not exist!");
											break;
									}
								} else player.sendMessage(ChatColor.RED + "You need to be in a settlement to build!");
								
								break;
								
							case "create":
								if(civ.getSettlement() == null)
								{
									if(args[1].length() <= 16)
									{
										if(!new SQLQuery(plugin).settlementExists(args[1]))
										{
											boolean cont = true;
											List<SettlementClaim> claims = new ArrayList<SettlementClaim>();
											for(Chunk chunk : getChunksAroundPlayer(player))
											{
												SettlementClaim sc = new SettlementClaim(plugin, chunk, args[1]);
												if (sc.claim()) {
													sc.setDistrict(DistrictType.CITY_CENTRE);
													claims.add(sc);
													continue;
												} else {
													player.sendMessage(ChatColor.RED + "An area nearby is already claimed by " + sc.getOwner() + "!");
													cont = false;
													break;
												}
											}
											
											if (cont) {
												Settlement settlement = plugin.getCache().createSettlement(player, args[1], claims);
												civ.setSettlement(settlement);
												new TechTree(plugin).grantTech("root", player);
												player.sendMessage(ChatColor.GREEN + "Settlement created! The surrounding you are standing in has been claimed.");
											}
										} else {
											player.sendMessage(ChatColor.RED + "This settlement already exists! Try coming up with a new name.");
										}
									} else {
										player.sendMessage(ChatColor.RED + "Your settlement name cannot be longer than 16 characters.");
									}
								} else {
									player.sendMessage(ChatColor.RED + "You are already in a settlement!");
								}
								
								break;
							
							case "research":
								if(civ.getSettlement() != null)
								{
									if(EnumUtils.isValidEnum(TechnologyType.class, args[1].toUpperCase()))
									{
										switch(args[1].toLowerCase())
										{
											case "writing":
												AdvancementProgress pottery = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/pottery")));
												if(pottery.isDone())
												{
													Technology tech = new Technology(plugin, civ.getSettlement(), player, 3, 200, TechnologyType.WRITING);
													tech.startResearch();
												} else player.sendMessage(ChatColor.RED + "You need to research Pottery first!");
												
												break;
												
											case "wheel":
												AdvancementProgress mining = player.getAdvancementProgress(Bukkit.getAdvancement(new NamespacedKey(plugin, "tech/mining")));
												if(mining.isDone())
												{
													Technology tech = new Technology(plugin, civ.getSettlement(), player, 3, 200, TechnologyType.WHEEL);
													tech.startResearch();
												} else player.sendMessage(ChatColor.RED + "You need to research Mining first!");
												
												break;
												
											default:
												Technology tech = new Technology(plugin, civ.getSettlement(), player, 3, 200, TechnologyType.valueOf(args[1].toUpperCase()));
												tech.startResearch();
												break;
										}
									} else {
										player.sendMessage(ChatColor.RED + "That is not a valid technology! Choose from one of the following: ");
										player.sendMessage(ChatColor.GOLD + " List of all technologies:");
										for(TechnologyType tech : TechnologyType.values())
										{
											player.sendMessage(ChatColor.GREEN + " - " + StringUtils.capitalize(tech.toString().toLowerCase()));
										}
										
										player.sendMessage(ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Note");
										player.sendMessage("Make sure to include underscores (_).");
									}
								} else player.sendMessage(ChatColor.RED + "You need to be in a settlement to research technologies!");
								
								break;
							
							case "invite":
								if(civ.getSettlement() != null)
								{
									Player target = Bukkit.getPlayer(args[1]);
									if(target != null && plugin.getCache().getPlayers().get(target.getUniqueId()).getSettlement() == null)
									{
										target.sendMessage(" ");
										target.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + player.getName() + " has invited you to join the Settlement of " + civ.getSettlement().getName() + "!");
										target.sendMessage(" ");
										TextComponent message = new TextComponent(ChatColor.YELLOW + " > " + ChatColor.GREEN + "Click here to accept the invitation.");
										message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sm accept " + civ.getSettlement().getName()));
										message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to accept the invitation!" ).create()));
										
										target.spigot().sendMessage(message);
										target.sendMessage(" ");
										
										player.sendMessage(ChatColor.GREEN + "The player has been invited!");
									} else player.sendMessage(ChatColor.RED + "That player is not online, or they are already in a settlement!");
								} else player.sendMessage(ChatColor.RED + "You are not in a settlement yet!");
								
								break;
								
							case "accept":
								
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									if(civ.getSettlement() == null)
									{
										if(new SQLQuery(plugin).settlementExists(args[1]))
										{
											Settlement settlement = plugin.getCache().getSettlement(args[1]);
											civ.setSettlement(settlement);
											civ.updateInfoBar();
											civ.sendMessage(ChatColor.GREEN + "You have joined the Settlement of " + settlement + "!");
											TechTree tech = new TechTree(plugin);
											tech.grantTech("root", player);
											
											for(OfflinePlayer op : settlement.getMembers())
											{
												if(op.isOnline())
												{
													CivPlayer member = plugin.getCache().getPlayers().get(op.getUniqueId());
													member.sendMessage(ChatColor.GREEN + player.getName() + " has joined your settlement.");
												}
											}
											
											TechTree tree = new TechTree(plugin);
											for(TechnologyType types : settlement.getTech())
											{
												tree.grantTech(types.getName(), player);
											}
											
											for(TechnologyType types : settlement.getProgresses())
											{
												tree.boost(types, types.getCriteria(), settlement);
											}
										} else civ.sendMessage(ChatColor.RED + "That settlement does not exist!");
									} else civ.sendMessage(ChatColor.RED + "You are already in a settlement!");
								});
								
								break;
								
							case "exile":
								Settlement settlement = civ.getSettlement();
								if(settlement != null)
								{
									if(settlement.isKing(player))
									{
										Player target = Bukkit.getPlayer(args[1]);
										if(target != player)
										{
											if(target != null)
											{
												player.sendMessage(ChatColor.RED + "Player has been exiled!");
												target.sendMessage(ChatColor.RED + "You have been exiled from the settlement of " + settlement.getName() + "!");
												target.playSound(target.getLocation(), Sound.BLOCK_ANVIL_LAND, 1, 1);
												settlement.exile(target);
											} else {
												settlement.exile(Bukkit.getOfflinePlayer(args[1]));
												player.sendMessage(ChatColor.RED + "Player has been exiled!");
											}
										} else player.sendMessage(ChatColor.RED + "You can't exile yourself!");
									} else player.sendMessage(ChatColor.RED + "You must be king of the settlement to do this!");
								} else player.sendMessage(ChatColor.RED + "You are not in a settlement yet!");
								
								break;
								
							default:
								sendHelp(player);
						}
						
					break;
						
					case 1:
						
						if(civ.getSettlement() != null)
						{
							switch(args[0].toLowerCase()) 
							{
								case "claim":
									SettlementClaim sc = new SettlementClaim(plugin, player.getLocation().getChunk(), civ.getSettlement().getName());
									if(sc.claim()) 
									{
										player.sendMessage(ChatColor.GREEN + "Area claimed!");
									} else player.sendMessage(ChatColor.RED + "This area has already been claimed by " + sc.getOwner() + "!");
									
									break;
									
								case "info":
									sendInfo(player, civ.getSettlement());
									break;
									
								case "help":
									sendHelp(player);
									break;
									
								case "disband":
									Settlement settlement = civ.getSettlement();
									
									if(settlement.isKing(player))
									{
										
										for(SettlementClaim claims : settlement.getClaims())
										{
											claims.setRuins(true);
											claims.removeOwner();
										}
										
										if (settlement.getTech() != null) {
											for (TechnologyType tech : settlement.getTech()) {	
												new TechTree(plugin).revokeTech(tech, player);
											}
										}
										
										new TechTree(plugin).revokeTech("root", player);
										
										plugin.getCache().deleteSettlement(settlement);
									} else player.sendMessage(ChatColor.RED + "You must be king of the settlement to do this!");
									
									break;
									
								case "research":
									player.sendMessage(getString(player, "opening-ui"));
									new TechUI(plugin, civ.getSettlement()).openInventory(plugin, player);
									break;
									
								case "view":
									
									SettlementClaim news = new SettlementClaim(plugin, player.getLocation().getChunk());
									if(news.getOwner() != null)
									{
										if(news.getOwner().equals(civ.getSettlement().getName()))
										{
											if(!cancel.containsKey(player))
											{
												final Location back = player.getLocation();
												Chunk chunk = player.getLocation().getChunk();
									            
									            for(Player pl : Bukkit.getOnlinePlayers())
									            {
									            	player.hidePlayer(plugin, pl);
									            }
									            
									            final GameMode gm = player.getGameMode();
									            
									            player.setGameMode(GameMode.ADVENTURE);
									            
									            Location center = new Location(chunk.getWorld(), chunk.getX() << 4, 64, chunk.getZ() << 4).add(7, 0, 7);
									            center.setY(center.getWorld().getHighestBlockYAt(center) + 23);
									            center.setPitch(90);
									            player.teleport(center);
									            
									            player.setInvulnerable(true);
									            player.setGravity(false);
									            player.setAllowFlight(true);
									            player.setFlying(true);
									            
									            civ.setViewing(true, back);
									            
									            List<Entity> holograms = new ArrayList<Entity>();
									            for(Chunk chunks : getChunksAroundPlayer(player))
								            	{
								            		Location loc = new Location(chunks.getWorld(), chunks.getX() << 4, 64, chunks.getZ() << 4).add(7, 0, 7);
								            		loc.setY(center.getY());
								            		
								            		for(Entity entities : chunks.getEntities())
								            		{
								            			if(entities instanceof Animals)
								            			{
								            				String addons = "";
								            				TechTree tech = new TechTree(plugin);
								            				if(!tech.hasTech("animalhusbandry", player)) addons = ChatColor.RED + " Requires " + ChatColor.UNDERLINE + "Animal Husbandry";
								            				ArmorStand as = spawnHologram(StringUtils.capitalize(entities.getType().toString().toLowerCase()).replace("_", " ") + addons, loc);
								            				loc.setY(loc.getY() - 0.3);
								            				
										            		holograms.add(as);
										            		plugin.getRemoveOnDisable().add(as);
								            			}
								            		}
								            	}
									            
									            final int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
									            	
									            	if(player.isOnline())
									            	{
										            	if(player.isSneaking())
										            	{
										            	
											            	player.setInvulnerable(false);
												            player.setGravity(true);
												            player.setAllowFlight(false);
												            player.setFlying(false);
												            
												            player.setGameMode(gm);
												            
												            player.teleport(back);
												            
												            for(Entity hologram : holograms)
												            {
												            	hologram.remove();
												            	plugin.getRemoveOnDisable().remove(hologram);
												            }

												            civ.setViewing(false, back);
												            Bukkit.getScheduler().cancelTask(cancel.get(player));
												            cancel.remove(player);
										            		
										            	} else {
											            	player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("Hold shift to exit"));
											            	if(player.getLocation().getY() > center.getY() || player.getLocation().getY() < center.getY()) 
											            	{
											            		Location prevent = center;
											            		prevent.setX(player.getLocation().getX());
											            		prevent.setZ(player.getLocation().getZ());
											            		player.teleport(center);
											            	}
											            	
											            	SettlementClaim current = new SettlementClaim(plugin, player.getLocation().getChunk());
											            	if(player.getLocation().distance(center) > 24)
											            	{
											            		player.teleport(center);
											            	} else {
												            	if(current.getOwner() == null)
												            	{
												            		player.teleport(center);
												            	} else if(!current.getOwner().equals(civ.getSettlement().getName())) player.teleport(center);
									            			}
										            	}
									            	} else {
									            		civ.setViewing(false, back);
									            		Bukkit.getScheduler().cancelTask(cancel.get(player));
											            cancel.remove(player);
									            	}
									            }, 0, 20);
									            
									            cancel.put(player, id);
									            
											} else player.sendMessage(ChatColor.RED + "You are already viewing!");
										} else player.sendMessage(ChatColor.RED + "You need to be in your own land to do that.");
									} else player.sendMessage(ChatColor.RED + "You need to be in your own land to do that.");
						            
						            break;
									
								default:
									player.sendMessage(ChatColor.RED + "Invalid arguments supplied. Try " + ChatColor.UNDERLINE + "/sm help" + ChatColor.RED + ".");
									break;
							}
						} else player.sendMessage(ChatColor.RED + "You are not in a settlement yet!");
						
					break;
					
					case 0:
						
						if(civ.getSettlement() != null)
						{
							sendInfo(player, civ.getSettlement());
						} else player.sendMessage(ChatColor.RED + "You are not in a settlement yet!");
						
						break;
					
					default:
						player.sendMessage(ChatColor.RED + "Invalid arguments supplied. Try " + ChatColor.UNDERLINE + "/sm help" + ChatColor.RED + ".");
						break;
				}
			} else sender.sendMessage("You must be a player to run this command.");	
		}
		
		return true;
	}
	
	public void sendInfo(Player player, Settlement settlement)
	{
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
			String name = settlement.getName();
			String king = settlement.getOwner().getName();
			String era = settlement.getEra().toString();
			String circa = settlement.getCirca();
			int level = settlement.getLevel();
			int science = settlement.getScience();
			int gold = settlement.getGold();
			long year = settlement.getYear();
			int turn = settlement.getTurn();
			int boosters = settlement.getBoosters();
			
			civ.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "The Settlement of " + name);
			civ.sendMessage(" ");
			civ.sendMessage(" " + ChatColor.GREEN + "King: " + ChatColor.LIGHT_PURPLE + king);
			civ.sendMessage(" " + ChatColor.GREEN + "Level: " + ChatColor.LIGHT_PURPLE + level);
			civ.sendMessage(" " + ChatColor.GREEN + "Science: " + ChatColor.LIGHT_PURPLE + science);
			civ.sendMessage(" " + ChatColor.GREEN + "Gold: " + ChatColor.LIGHT_PURPLE + gold);
			civ.sendMessage(" " + ChatColor.GREEN + "Year: " + ChatColor.LIGHT_PURPLE + year + " " + circa);
			civ.sendMessage(" " + ChatColor.GREEN + "Turn: " + ChatColor.LIGHT_PURPLE + turn);
			civ.sendMessage(" " + ChatColor.GREEN + "Era: " + ChatColor.LIGHT_PURPLE + era);
			civ.sendMessage(" " + ChatColor.GREEN + "Boosters: " + ChatColor.LIGHT_PURPLE + boosters);
			
			List<String> members = new ArrayList<String>();
			for(OfflinePlayer op : settlement.getMembers())
			{
				if(op != settlement.getOwner()) members.add(op.getName());
			}
			
			civ.sendMessage(" " + ChatColor.GREEN + "Settlers " + ChatColor.AQUA + "[" + members.size() + "]" + ChatColor.GREEN + ": " + ChatColor.LIGHT_PURPLE + Arrays.asList(members).toString().replace("[", "").replace("]", ""));
		});
	}
	
	public void sendHelp(CommandSender player) {
		player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Settlement Commands:");
		player.sendMessage(" ");
		player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.GREEN + "Claim:");
		player.sendMessage(ChatColor.WHITE + "    Claim the area you are in.");
		player.sendMessage(ChatColor.WHITE + "    Costs 4 resources.");
		player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.GREEN + "Info:");
		player.sendMessage(ChatColor.WHITE + "    Information about your settlement.");
		player.sendMessage(ChatColor.WHITE + "    You can also use /settlement or /sm.");
		player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.GREEN + "Create [name]:");
		player.sendMessage(ChatColor.WHITE + "    Create a new settlement.");
		player.sendMessage(ChatColor.WHITE + "    A name must be provided.");
	}
	
	public String getString(Player player, String path) {
		return ChatColor.translateAlternateColorCodes('&', plugin.getTranslator().getTranslationFor(player, path));
	}
	
	public ArmorStand spawnHologram(String name, Location location) {
		ArmorStand as = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
		as.setGravity(false);
		as.setCanPickupItems(false);
		as.setCustomName(name);
		as.setCustomNameVisible(true);
		as.setVisible(false);
		return as;
	}
	
	public List<Chunk> getChunksAroundPlayer(Player player) {
		int[] offset = {-1,0,1};

		World world = player.getWorld();
		int ox = player.getLocation().getChunk().getX();
		int oz = player.getLocation().getChunk().getZ();

		List<Chunk> chunks = new ArrayList<Chunk>();
        for (int x : offset) {
        	for (int z : offset) {
        		Chunk chunk = world.getChunkAt(ox + x, oz + z);
        		chunks.add(chunk);
        	}
        } return chunks;
	}
}
