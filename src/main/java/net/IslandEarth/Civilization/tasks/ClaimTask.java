package net.islandearth.civilization.tasks;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.settlements.Settlement;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.tech.TechTree;
import net.islandearth.civilization.tech.TechnologyType;

public class ClaimTask implements Runnable {

	private Civilization plugin;
	private Map<UUID, String> players = new HashMap<>();
	
	public ClaimTask(Civilization plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			Chunk chunk = player.getLocation().getChunk();
			SettlementClaim sc = new SettlementClaim(plugin, chunk);
			if (players.containsKey(player.getUniqueId())) {
				if (sc.isRuins() && sc.getRuinsOwner() != null) {
					if (players.get(player.getUniqueId()).equals(sc.getRuinsOwner())) return;
				} else if (!sc.isRuins() && sc.getOwner() != null) {
					if (players.get(player.getUniqueId()).equals(sc.getOwner())) return;
				} else {
					players.remove(player.getUniqueId());
				}
			}
			
			CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
			if (sc.isRuins()) {
				if (sc.getRuinsOwner() != null) {
					civ.sendTitle(ChatColor.GRAY + "Ruins of " + sc.getRuinsOwner(), 40, 100, 40);
					players.put(player.getUniqueId(), sc.getRuinsOwner());
				}
			} else {
				if (sc.getOwner() != null) {
					players.put(player.getUniqueId(), sc.getOwner());
					if (civ.getSettlement() != null) {
						Settlement settlement = civ.getSettlement();
						if (!settlement.hasTech(TechnologyType.WRITING) && !sc.getOwner().equals(settlement.getName())) {
							new TechTree(plugin).boost(TechnologyType.WRITING, "meet", settlement);
						}
						
						if (sc.getOwner().equals(settlement.getName())) civ.sendFullTitle(ChatColor.GREEN + sc.getOwner(), StringUtils.capitalize(sc.getDistrict().toString().toLowerCase().replace("_", " ")), 40, 100, 40);
						else civ.sendFullTitle(ChatColor.GRAY + sc.getOwner(), StringUtils.capitalize(StringUtils.capitalize(sc.getDistrict().toString().toLowerCase().replace("_", " ")).toLowerCase().replace("_", " ")), 40, 100, 40);

					} else {
						civ.sendFullTitle(ChatColor.GRAY + sc.getOwner(), StringUtils.capitalize(StringUtils.capitalize(sc.getDistrict().toString().toLowerCase().replace("_", " ")).toLowerCase().replace("_", " ")), 40, 100, 40);
					}
				}
			}
		}
	}
}
