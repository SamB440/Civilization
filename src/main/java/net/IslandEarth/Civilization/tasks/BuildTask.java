package net.islandearth.civilization.tasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.BuildingType;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.schematic.Scheduler;
import net.islandearth.civilization.schematic.Schematic;
import net.islandearth.civilization.schematic.Schematic.Options;
import net.islandearth.civilization.settlements.SettlementClaim;
import net.islandearth.civilization.schematic.SchematicNotLoadedException;

@RequiredArgsConstructor
public class BuildTask {
	
	@NonNull
	private Civilization plugin;
	
	@NonNull
	private Player player;
	
	private Scheduler scheduler;
	
	@Getter 
	private Map<Player, List<Location>> cache = new HashMap<>();
	
	@SuppressWarnings("deprecation")
	public BuildTask start() {
		plugin.getBuildTask().put(player.getUniqueId(), this);
		scheduler = new Scheduler();
		scheduler.setTask(
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, () -> {
			CivPlayer civ = plugin.getCache().getPlayers().get(player.getUniqueId());
			SettlementClaim sc = new SettlementClaim(plugin, player.getTargetBlock(null, 10).getChunk());
			if (civ.isBuilding() && sc.isClaimed() && sc.getDistrict() != null) {
				if (civ.getBuildingType() == BuildingType.FORGE) {
					Schematic schematic = civ.getBuilding();
					try {
						List<Location> locations = schematic.pasteSchematic(player.getTargetBlock(null, 10).getLocation().add(0, 1, 0), player, Options.PREVIEW);
						Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
							if (cache.containsKey(player)) {
								if (!cache.get(player).equals(locations)) {
									int current = 0;
									for (Location location : cache.get(player)) {
										if (current == locations.size()) break;
										if (location.distance(locations.get(current)) >= 1) player.sendBlockChange(location, location.getBlock().getBlockData());
										current++;
									}
									cache.remove(player);
								}
							}
						if(!cache.containsKey(player)) cache.put(player, locations);
						}, 2L);
					} catch (SchematicNotLoadedException e) {
						e.printStackTrace();
					}
				}
			} else if (!civ.isBuilding()) {
				scheduler.cancel();
				plugin.getBuildTask().remove(player.getUniqueId());
			}
		}, 5L, 1L));
		
		return this;
	}
}
