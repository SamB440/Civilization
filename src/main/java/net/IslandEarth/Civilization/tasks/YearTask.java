package net.islandearth.civilization.tasks;

import org.bukkit.OfflinePlayer;

import lombok.AllArgsConstructor;
import net.islandearth.civilization.Civilization;
import net.islandearth.civilization.buildings.Building;
import net.islandearth.civilization.player.CivPlayer;
import net.islandearth.civilization.settlements.Settlement;

@AllArgsConstructor
public class YearTask implements Runnable {

	private Civilization plugin;
	
	@Override
	public void run() {
		for (Settlement settlement : plugin.getCache().getSettlements().values()) {
			if (settlement != null) {
				int current = settlement.getTurn();
				long currentyear = settlement.getYear();
				String circa = settlement.getCirca();
				long newyear = 0;
				if (circa.equals("BC")) settlement.setYear(newyear = currentyear - 1);
				else if (circa.equals("AD")) settlement.setYear(newyear = currentyear + 1);
				if (currentyear == 0 && circa.equals("BC")) settlement.setCirca("AD");
				
				int newturn = current + 1;
				settlement.setTurn(newturn);
				
				int science = settlement.getScience() + settlement.getMembers().size();
				settlement.setScience(science);
				
				for (OfflinePlayer op : settlement.getMembers()) {
					if (op.isOnline()) {
						CivPlayer civ = plugin.getCache().getPlayers().get(op.getUniqueId());
						civ.sendFullTitle("Turn " + newturn, newyear + " " + settlement.getCirca(), 40, 100, 40);
					}
				}
				
				if (settlement.getBuildings() != null) {
					for (Building type : settlement.getBuildings()) {
						settlement.setCulture(settlement.getCulture() + type.getType().getCulture());
					}
				}
			}
		}
	}
}
